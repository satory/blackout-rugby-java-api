README
ver1.0

The purpose of this project is to create a Java API adapter for the Blackout Rugby API.  This is the first step towards creating a possible Mobile Application for Blackout Rugby

Requirements:  
1. This project requires a dev.properties file to be created with values for the following keys:
dev.id, dev.iv, dev.key

2. For tests, this project requires multiple properties to be declared in the dev.properties file.  Please refer to the
pom file for a list of properties that are needed.