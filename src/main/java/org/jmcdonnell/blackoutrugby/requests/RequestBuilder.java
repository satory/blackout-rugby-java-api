/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import java.util.List;

/**
 *
 * @author john
 */
public class RequestBuilder {

    private static RequestBuilder builder;

    static RequestBuilder initBuilderForPlayersByTeam() {
        builder = new RequestBuilder();        
        
        builder.requestStringBuilder.append("p&teamid");
        
        return builder;
    }
    static RequestBuilder initBuildForFixturesForTeam() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("f&teamid");
        
        return builder;
    }
    
    static RequestBuilder initBuilderForFixtures() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("f&fixtureid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForMatchScore() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("ls&fixtureid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForPlayerStatistics() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("ps&playerid");
        
        return builder;
    }
    
    public static RequestBuilder initBuilderForPlayerHistory() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("ph&playerid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForMatchEvent() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("me&fixtureid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForLineups() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("lu&teamid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForStandings() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("s&leagueid");
        
        return builder;
    }

    public static RequestBuilder initBuilderForTeamStatistics() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("ts&teamid");
        
        return builder;
    }
    
    private StringBuilder requestStringBuilder;
    
    public static RequestBuilder init() {
        builder = new RequestBuilder();
        
        return builder;
    }
    
    public static RequestBuilder initBuilderForMembers() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("m&memberid");
        
        return builder;
    }
    
    public static RequestBuilder initBuilderForTeams() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("t&teamid");
        
        return builder;
    }
    
    public static RequestBuilder initBuilderForPlayers() {
        builder = new RequestBuilder();
        
        builder.requestStringBuilder.append("p&playerid");
        
        return builder;
    }
    
    private RequestBuilder() {
        requestStringBuilder = new StringBuilder("&r=");
    }
    
    public String build()
    {
        return requestStringBuilder.toString();
    }

    public void createLoginRequest(String username, String password) {
        
        // for login requests we dont need the leading '&'
        requestStringBuilder.deleteCharAt(0);
        
        requestStringBuilder.append("lgn");
        requestStringBuilder.append("&username=");
        requestStringBuilder.append(username);
        requestStringBuilder.append("&password=");
        requestStringBuilder.append(password);
    }

    public void addId(Long entityId) {
        requestStringBuilder.append("=").append(entityId.toString());
    }
    
    public void addIds(List<Long> entitiesId) {
        requestStringBuilder.append("s=");
        for (Long id : entitiesId) {
            requestStringBuilder.append(id.toString());
            requestStringBuilder.append(",");
        }
        
        // strip the last comma away
        requestStringBuilder.deleteCharAt(requestStringBuilder.lastIndexOf(","));
    }

    public void andAddFixtureId(Long fixtureId) {
        requestStringBuilder.append("&fixtureid=");
        requestStringBuilder.append(fixtureId.toString());
    }

    public void addTeamSettings(Boolean nationalTeam, Boolean youthTeam) {
        
        if (nationalTeam && youthTeam) {
            requestStringBuilder.append("&u20=1");
        }else if (nationalTeam) {
            requestStringBuilder.append("&nat=1");
        } else if (youthTeam) {
            requestStringBuilder.append("&youth=1");
        }
    }
    
    public void addLastX(Integer number) {
        requestStringBuilder.append("&last=").append(number);
    }
}