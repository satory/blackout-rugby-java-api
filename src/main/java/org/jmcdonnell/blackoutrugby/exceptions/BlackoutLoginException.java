/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.exceptions;

public class BlackoutLoginException extends BlackoutException 
{
    public BlackoutLoginException(String msg)
    {
        super(msg);
    }
}