/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.exceptions;

/**
 *
 * @author john
 */
public class BlackoutException extends Exception 
{
    /**
     * Constructs an instance of
     * <code>BlackoutException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public BlackoutException(String msg) {
        super(msg);
    }
}