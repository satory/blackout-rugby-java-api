/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.BlackoutLoginDetails;
import org.jmcdonnell.blackoutrugby.beans.Fixture;
import org.jmcdonnell.blackoutrugby.beans.Lineup;
import org.jmcdonnell.blackoutrugby.beans.Member;
import org.jmcdonnell.blackoutrugby.beans.Player;
import org.jmcdonnell.blackoutrugby.beans.PlayerStatistics;
import org.jmcdonnell.blackoutrugby.beans.Standing;
import org.jmcdonnell.blackoutrugby.beans.Team;
import org.jmcdonnell.blackoutrugby.beans.TeamStatistics;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutLoginException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;
import org.jmcdonnell.blackoutrugby.services.fixtures.IFixtureService;
import org.jmcdonnell.blackoutrugby.services.lineups.ILineupService;
import org.jmcdonnell.blackoutrugby.services.members.IMemberService;
import org.jmcdonnell.blackoutrugby.services.players.IPlayerService;
import org.jmcdonnell.blackoutrugby.services.standing.IStandingService;
import org.jmcdonnell.blackoutrugby.services.statistics.IStatisticsService;
import org.jmcdonnell.blackoutrugby.services.team.ITeamService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Public API for Blackout Rugby.
 */
public class BlackoutApi implements IMemberService, 
        ITeamService, 
        IPlayerService,
        IFixtureService,
        IStandingService, 
        IStatisticsService,
        ILineupService {

    private RequestManager requestManager;
    private IMemberService memberService;
    private ITeamService teamService;
    private IPlayerService playerService;
    private IFixtureService fixtureService;
    private IStandingService standingService;
    private IStatisticsService statisticsService;
    private ILineupService lineupService;
    
    private static BlackoutApi instance;
    protected BlackoutLoginDetails loggedInMemberDetails;

    private BlackoutApi() {
        BeanFactory factory = new ClassPathXmlApplicationContext("META-INF/beans.xml");
        setRequestManager((RequestManager) factory.getBean("requestManager"));
        setTeamService((ITeamService) factory.getBean("teamService"));
        setPlayerService((IPlayerService) factory.getBean("playerService"));
        setMemberService((IMemberService) factory.getBean("memberService"));
        setStandingService((IStandingService) factory.getBean("standingService"));
        setStatisticsService((IStatisticsService) factory.getBean("statisticsService"));
        setFixtureService((IFixtureService) factory.getBean("fixtureService"));
        setLineupService((ILineupService) factory.getBean("lineupService"));
    }

    //<editor-fold defaultstate="collapsed" desc="Serivce Accessor Methods">
    public final void setRequestManager(RequestManager requestManager) {
        this.requestManager = requestManager;
    }
    
    public final void setMemberService(IMemberService memberService) {
        this.memberService = memberService;
    }
    
    public final void setPlayerService(IPlayerService playerService) {
        this.playerService = playerService;
    }
    
    public final void setTeamService(ITeamService teamService) {
        this.teamService = teamService;
    }
    
    public final void setFixtureService(IFixtureService fixtureService) {
        this.fixtureService = fixtureService;
    }
    
    public final void setStandingService(IStandingService standingService) {
        this.standingService = standingService;
    }

    public final void setStatisticsService(IStatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    public final void setLineupService(ILineupService lineupService) {
        this.lineupService = lineupService;
    }
    //</editor-fold>
    
    /**
     * Initializes an instance of the API when the user provides correct log in credentials.
     * @param username the username of the user.
     * @param password the password of the user.
     * @return an instance of the API for the given user.
     * @throws BlackoutLoginException if an error occurs while logging in.
     */
    public static BlackoutApi init(String username, String password) throws BlackoutException
    {
        instance =  new BlackoutApi();
        
        instance.login(username, password);
        
        return instance;
    }

    private void login(String username, String password) throws BlackoutLoginException, BlackoutException {
        setLoginInMemberDetails(requestManager.login(username, password));
    }

    public Long getMemberId() {
        return loggedInMemberDetails.getMemberId();
    }

    public Long getTeamId() {
        return loggedInMemberDetails.getTeamId();
    }

    public String getMemberApiKey() {
        return loggedInMemberDetails.getKey();
    }

    protected void setLoginInMemberDetails(BlackoutLoginDetails login) {
        loggedInMemberDetails = login;
    }
    
    public Member getLoggedInMember() throws BlackoutException {
        if (loggedInMemberDetails == null || getMemberId() == null)
        {
            throw new BlackoutLoginException("User is not logged in.");
        }
        return memberService.getMemberById(getMemberId());
    }

    //<editor-fold defaultstate="collapsed" desc="Member Related API Methods">
    @Override
    public Member getMemberById(Long memberId) throws BlackoutException {
        return memberService.getMemberById(memberId);
    }

    @Override
    public List<Member> getMembersByIds(List<Long> memberIds) throws BlackoutException {
        return memberService.getMembersByIds(memberIds);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Team Related API Methods">
    @Override
    public Team getTeamById(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return teamService.getTeamById(teamId, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Team> getTeamsByIds(List<Long> teamIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return teamService.getTeamsByIds(teamIds, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Player Related API Methods">
    @Override
    public Player getPlayerById(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return playerService.getPlayerById(playerId, isNationalTeam, isYouthTeam);
    }
    
    @Override
    public List<Player> getPlayersByIds(List<Long> playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return playerService.getPlayersByIds(playerIds, isNationalTeam, isYouthTeam);
    }
    
    @Override
    public List<Player> getPlayersInTeamSquad(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return playerService.getPlayersInTeamSquad(teamId, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fixture Related API Methods">
    @Override
    public Fixture getFixtureById(Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return fixtureService.getFixtureById(fixtureId, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Fixture> getFixturesByIds(List<Long> fixtureIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return fixtureService.getFixturesByIds(fixtureIds, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Standing Related API Methods">
    @Override
    public List<Standing> getStandingsForLeagueId(Long leagueId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return standingService.getStandingsForLeagueId(leagueId, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Statistics Related API Methods">
    @Override
    public PlayerStatistics getPlayerStatisticsByPlayerId(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return statisticsService.getPlayerStatisticsByPlayerId(playerId, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<PlayerStatistics> getPlayerStatisticsByPlayerIds(List<Long> playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return statisticsService.getPlayerStatisticsByPlayerIds(playerIds, isNationalTeam, isYouthTeam);
    }

    @Override
    public TeamStatistics getTeamStatisticsByTeamId(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return statisticsService.getTeamStatisticsByTeamId(teamId, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Lineup Related API Methods">
    @Override
    public Lineup getDefaultLineupForTeam(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return lineupService.getDefaultLineupForTeam(teamId, isNationalTeam, isYouthTeam);
    }

    @Override
    public Lineup getLineUpForTeamFromFixture(Long teamId, Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return lineupService.getLineUpForTeamFromFixture(teamId, fixtureId, isNationalTeam, isYouthTeam);
    }
    //</editor-fold>
}