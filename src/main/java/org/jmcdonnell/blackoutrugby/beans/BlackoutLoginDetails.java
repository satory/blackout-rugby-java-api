/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

/**
 *
 * @author john
 */
public class BlackoutLoginDetails {
    private String status;
    private String key;
    private Long memberId;
    private Long teamId;
    private String reason;

    public final String getStatus() {
        return status;
    }

    public final void setStatus(String status) {
        this.status = status;
    }

    public final String getKey() {
        return key;
    }

    public final void setKey(String key) {
        this.key = key;
    }

    public final Long getMemberId() {
        return memberId;
    }

    public final void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public final Long getTeamId() {
        return teamId;
    }

    public final void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public final String getReason() {
        return reason;
    }

    public final void setReason(String reason) {
        this.reason = reason;
    }
}
