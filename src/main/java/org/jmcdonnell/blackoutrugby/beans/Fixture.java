/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@XmlRootElement(name = "fixture")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fixture extends AbstractBlackoutEntity {
    
    @XmlElement(name = "id")
    private Long fixtureId;
    @XmlElement(name = "season")
    private Integer season;
    @XmlElement(name = "country_iso")
    private String countryIso;
    @XmlElement(name = "leagueid")
    private Long leagueId;
    @XmlElement(name = "round")
    private Integer round;
    @XmlElement(name = "hometeamid")
    private Long homeTeamId;
    @XmlElement(name = "guestteamid")
    private Long guestTeamId;
    @XmlElement(name = "competition")
    private String competition;
    @XmlElement(name = "botmatch")
    private Integer botMatch;
    @XmlElement(name = "matchstart")
    private Long matchStart;
    

    /**
     * {@inheritDoc}
     */
    public Long getGuestTeamId() {
        return guestTeamId;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getBotMatch() {
        return botMatch;
    }


    /**
     * {@inheritDoc}
     */
    public String getCompetition() {
        return competition;
    }

    /**
     * {@inheritDoc}
     */
    public String getCountryIso() {
        return countryIso;
    }

    /**
     * {@inheritDoc}
     */
    public Long getFixtureId() {
        return fixtureId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getHomeTeamId() {
        return homeTeamId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getLeagueId() {
        return leagueId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getMatchStart() {
        return matchStart;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getRound() {
        return round;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getSeason() {
        return season;
    }
}