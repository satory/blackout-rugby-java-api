/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 *
 * @author john
 */
public class TeamTraining {
    @XmlElementWrapper(name = "skills")
    List<TeamTrainingSkill> skills;
    @XmlElementWrapper(name = "players")
    List<TeamTrainingPlayer> players;
}
