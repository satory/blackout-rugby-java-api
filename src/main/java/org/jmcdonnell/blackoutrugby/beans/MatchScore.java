/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.BooleanAdapter;

@XmlRootElement(name = "livescoring")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchScore extends AbstractBlackoutEntity {
    
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    @XmlElement(name = "match_has_started")
    private Boolean isMatchStarted;
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    @XmlElement(name = "match_has_finished")
    private Boolean isMatchFinished;
    
    @XmlElement(name = "homescore")
    private Integer homeScore;
    @XmlElement(name = "guestscore")
    private Integer guestScore;
    @XmlElement(name = "gametime")
    private Integer gameTimeInSecs;

    public Integer getHomeScore() {
        return homeScore;
    }

    public Integer getGuestScore() {
        return guestScore;
    }

    public Integer getGameTimeInSecs() {
        return gameTimeInSecs;
    }

    public Boolean isMatchStarted() {
        return isMatchStarted;
    }

    public Boolean isMatchFinished() {
        return isMatchFinished;
    }
}