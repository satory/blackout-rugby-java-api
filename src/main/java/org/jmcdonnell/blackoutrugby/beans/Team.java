/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.BooleanAdapter;

/**
 *
 * @author john
 */
@XmlRootElement(name = "team")
@XmlAccessorType(XmlAccessType.FIELD)
public class Team extends AbstractBlackoutEntity {
    @XmlElement(name = "id")
    private Long teamId;
    @XmlElement(name = "owner")
    private Long ownerId;
    @XmlElement(name = "name")
    private String teamName;
    @XmlElement(name = "country_iso")
    private String countryIso;
    private Integer region;
    private String stadium;
    @XmlElement(name = "bot")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private Boolean bot;
    private Integer contentment;
    private Long members;
    @XmlElement(name = "stadium_capacity")
    private Long stadiumCapacity;
    @XmlElement(name = "stadium_standing")
    private Long stadiumStanding;
    @XmlElement(name = "stadium_uncovered")
    private Long stadiumUncovered;
    @XmlElement(name = "stadium_covered")
    private Long stadiumCovered;
    @XmlElement(name = "stadium_members")
    private Long stadiumMembers;
    @XmlElement(name = "stadium_corporate")
    private Long stadiumCorporate;
    @XmlElement(name = "bank_balance")
    private Long bankBalance;
    @XmlElement(name = "nickname_1")
    private String nickName1;
    @XmlElement(name = "nickname_2")
    private String nickName2;
    @XmlElement(name = "nickname_3")
    private String nickName3;
    @XmlElement(name = "ranking_points")
    private Double rankingPoints;
    @XmlElement(name = "regional_rank")
    private Integer regionRank;
    @XmlElement(name = "national_rank")
    private Integer nationalRank;
    @XmlElement(name = "world_rank")
    private Integer worldRank;
    @XmlElement(name = "minor_sponsors")
    private Integer minorSponsor;
    @XmlElement(name = "prev_ranking_points")
    private Double prevRankingPoints;
    @XmlElement(name = "prev_regional_rank")
    private Integer prevRegionRank;
    @XmlElement(name = "prev_national_rank")
    private Integer prevNationalRank;
    @XmlElement(name = "prev_world_rank")
    private Integer prevWorldRank;
    @XmlElement(name = "scouting_stars")
    private Integer scoutingStars;
    @XmlElement(name = "plural")
    private Integer plural;
    @XmlElement(name = "plural_nickname_1")
    private Integer pluralNickname1;
    @XmlElement(name = "plural_nickname_2")
    private Integer pluralNickname2;
    @XmlElement(name = "plural_nickname_3")
    private Integer pluralNickname3;
    @XmlElement(name = "minimum_salary")
    private Long minSalary;
    @XmlElement(name = "premium")
    private Integer premium;
    @XmlElement(name = "date_taken_over")
    private String dateTakenOver;
    @XmlElement(name = "leagueid")
    private Long leagueId;
    private Long manager;
    
    private Integer drift;
    private Integer rush;
    @XmlElement(name = "man_on_man")
    private Integer manOnMan;
    @XmlElement(name = "major_sponsor")
    private Integer majorSponsor;
    
    /**
     * {@inheritDoc}
     */
    public Boolean isBot() {
        return bot;
    }

    /**
     * {@inheritDoc}
     */
    public Long getLeagueId() {
        return leagueId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getManager() {
        return manager;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getNationalRank() {
        return nationalRank;
    }

    /**
     * {@inheritDoc}
     */
    public String getNickName1() {
        return nickName1;
    }

    /**
     * {@inheritDoc}
     */
    public String getNickName2() {
        return nickName2;
    }

    /**
     * {@inheritDoc}
     */
    public String getNickName3() {
        return nickName3;
    }

    /**
     * {@inheritDoc}
     */
    public Double getRankingPoints() {
        return rankingPoints;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getRegion() {
        return region;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getRegionRank() {
        return regionRank;
    }

    /**
     * {@inheritDoc}
     */
    public String getStadium() {
        return stadium;
    }

    /**
     * {@inheritDoc}
     */
    public Long getTeamId() {
        return teamId;
    }

    /**
     * {@inheritDoc}
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getWorldRank() {
        return worldRank;
    }

    /**
     * {@inheritDoc}
     */
    public Long getBankBalance() {
        return bankBalance;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getDrift() {
        return drift;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getRush() {
        return rush;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getManOnMan() {
        return manOnMan;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getContentment() {
        return contentment;
    }

    /**
     * {@inheritDoc}
     */
    public Long getMembers() {
        return members;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumCapacity() {
        return stadiumCapacity;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumStanding() {
        return stadiumStanding;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumUncovered() {
        return stadiumUncovered;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumCovered() {
        return stadiumCovered;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumMembers() {
        return stadiumMembers;
    }

    /**
     * {@inheritDoc}
     */
    public Long getStadiumCorporate() {
        return stadiumCorporate;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getMajorSponsor() {
        return majorSponsor;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getMinorSponsor() {
        return minorSponsor;
    }

    /**
     * {@inheritDoc}
     */
    public Double getPrevRankingPoints() {
        return prevRankingPoints;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPrevRegionRank() {
        return prevRegionRank;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPrevNationalRank() {
        return prevNationalRank;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPrevWorldRank() {
        return prevWorldRank;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getScoutingStars() {
        return scoutingStars;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPlural() {
        return plural;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPluralNickname1() {
        return pluralNickname1;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPluralNickname2() {
        return pluralNickname2;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getPluralNickname3() {
        return pluralNickname3;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public String getCountryIso() {
        return countryIso;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public Integer getPremium() {
        return premium;
    }

    public String getDateTakenOver() {
        return dateTakenOver;
    }
}
