/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "team_statistics")
@XmlAccessorType(XmlAccessType.FIELD)
public class TeamStatistics extends AbstractBlackoutEntity {
    
    @XmlElement(name = "teamid")
    private Long teamId;
    private Integer tackles;
    @XmlElement(name = "metresgained")
    private Integer metresGained;
    private Integer tries;
    private Integer conversions;
    @XmlElement(name = "dropgoals") 
    private Integer dropGoals;
    private Integer penalties;
    @XmlElement(name = "totalpoints")
    private Integer totalPoints;
    @XmlElement(name = "yellowcards")
    private Integer yellowCards;
    @XmlElement(name = "redcards") 
    private Integer redCards;
    @XmlElement(name = "linebreaks")
    private Integer lineBreaks;
    private Integer intercepts;
    private Integer kicks;
    @XmlElement(name = "knockons")
    private Integer knockOns;
    @XmlElement(name = "forwardpasses")
    private Integer forwardPasses;
    private Integer phases;
    @XmlElement(name = "sevenplusphases")
    private Integer sevenPlusPhases;
    private Integer turnovers;
    @XmlElement(name = "possession")
    private Double possession;
    @XmlElement(name = "territory")
    private Double territory;
    @XmlElement(name = "minutesintwentytwo")
    private Integer minsInTwentyTwo;
    @XmlElement(name = "lineoutswon")
    private Integer lineoutsWon;
    @XmlElement(name = "lineoutslost")
    private Integer lineoutsLost;
    @XmlElement(name = "lineoutsagainstthrow")
    private Integer lineoutsAgainstThrow;
    @XmlElement(name = "scrumswon")
    private Integer scrumsWon;
    @XmlElement(name = "scrumslost")
    private Integer scrumsLost;
    @XmlElement(name = "scrumsagainstputin")
    private Integer scrumsAgainstPutIn;
    @XmlElement(name = "ruckswon")
    private Integer rucksWon;
    @XmlElement(name = "maulswon")
    private Integer maulsWon;
    @XmlElement(name = "matchesplayed")
    private Integer matchesPlayed;
    private Integer injuries;
    @XmlElement(name = "handlingerrors")
    private Integer handlingErrors;
    @XmlElement(name = "missedtackles")
    private Integer missedTackles;
    private Integer fights;
    @XmlElement(name = "kickingmetres")
    private Integer kickingMetres;
    @XmlElement(name = "avminutesintwentytwo")
    private Double averageMinsInTwentyTwo;
    @XmlElement(name = "avkickingmetres")
    private Double averageKickingMetres;
    @XmlElement(name = "penaltiesconceded")
    private Integer penaltiesConceded;
    @XmlElement(name= "kicksoutonthefull")
    private Integer kicksOutOnTheFull;
    @XmlElement(name = "balltime")
    private Integer ballTime;
    @XmlElement(name = "penaltytime")
    private Integer penaltyTime;
    @XmlElement(name = "missedconversions")
    private Integer missedConversions;
    @XmlElement(name = "misseddropgoals")
    private Integer missedDropGoals;
    @XmlElement(name = "missedpenalties")
    private Integer missedPenalties;
    @XmlElement(name = "goodupandunders")
    private Integer goodUpAndUnders;
    @XmlElement(name = "badupandunders")
    private Integer badUpAndUnders;
    @XmlElement(name = "upandunders")
    private Integer upAndUnders;
    @XmlElement(name = "goodkicks")
    private Integer goodKicks;
    @XmlElement(name = "badkicks")
    private Integer badKicks;
    @XmlElement(name = "penaltieswon")
    private Integer penaltiesWon;
    @XmlElement(name = "turnoversconceded")
    private Integer turnoversConceded;

    public Long getTeamId() {
        return teamId;
    }

    public Integer getTackles() {
        return tackles;
    }

    public Integer getMetresGained() {
        return metresGained;
    }

    public Integer getTries() {
        return tries;
    }

    public Integer getConversions() {
        return conversions;
    }

    public Integer getDropGoals() {
        return dropGoals;
    }

    public Integer getPenalties() {
        return penalties;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public Integer getYellowCards() {
        return yellowCards;
    }

    public Integer getRedCards() {
        return redCards;
    }

    public Integer getLineBreaks() {
        return lineBreaks;
    }

    public Integer getIntercepts() {
        return intercepts;
    }

    public Integer getKicks() {
        return kicks;
    }

    public Integer getKnockOns() {
        return knockOns;
    }

    public Integer getForwardPasses() {
        return forwardPasses;
    }

    public Integer getInjuries() {
        return injuries;
    }

    public Integer getHandlingErrors() {
        return handlingErrors;
    }

    public Integer getMissedTackles() {
        return missedTackles;
    }

    public Integer getFights() {
        return fights;
    }

    public Integer getKickingMetres() {
        return kickingMetres;
    }

    public Double getAverageKickingMetres() {
        return averageKickingMetres;
    }

    public Integer getPenaltiesConceded() {
        return penaltiesConceded;
    }

    public Integer getKicksOutOnTheFull() {
        return kicksOutOnTheFull;
    }

    public Integer getBallTime() {
        return ballTime;
    }

    public Integer getPenaltyTime() {
        return penaltyTime;
    }

    public Integer getMissedConversions() {
        return missedConversions;
    }

    public Integer getMissedDropGoals() {
        return missedDropGoals;
    }

    public Integer getMissedPenalties() {
        return missedPenalties;
    }

    public Integer getGoodUpAndUnders() {
        return goodUpAndUnders;
    }

    public Integer getBadUpAndUnders() {
        return badUpAndUnders;
    }

    public Integer getUpAndUnders() {
        return upAndUnders;
    }

    public Integer getGoodKicks() {
        return goodKicks;
    }

    public Integer getBadKicks() {
        return badKicks;
    }

    public Integer getPhases() {
        return phases;
    }

    public Integer getSevenPlusPhases() {
        return sevenPlusPhases;
    }

    public Integer getTurnovers() {
        return turnovers;
    }

    public Double getPossession() {
        return possession;
    }

    public Double getTerritory() {
        return territory;
    }

    public Integer getMinsInTwentyTwo() {
        return minsInTwentyTwo;
    }

    public Integer getLineoutsWon() {
        return lineoutsWon;
    }

    public Integer getLineoutsLost() {
        return lineoutsLost;
    }

    public Integer getLineoutsAgainstThrow() {
        return lineoutsAgainstThrow;
    }

    public Integer getScrumsWon() {
        return scrumsWon;
    }

    public Integer getScrumsLost() {
        return scrumsLost;
    }

    public Integer getScrumsAgainstPutIn() {
        return scrumsAgainstPutIn;
    }

    public Integer getRucksWon() {
        return rucksWon;
    }

    public Integer getMaulsWon() {
        return maulsWon;
    }

    public Integer getMatchesPlayed() {
        return matchesPlayed;
    }

    public Double getAverageMinsInTwentyTwo() {
        return averageMinsInTwentyTwo;
    }

    public Integer getPenaltiesWon() {
        return penaltiesWon;
    }

    public Integer getTurnoversConceded() {
        return turnoversConceded;
    }
}