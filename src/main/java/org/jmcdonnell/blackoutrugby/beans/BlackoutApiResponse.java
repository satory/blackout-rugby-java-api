/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@XmlRootElement(name = "blackoutrugby_api_response")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlackoutApiResponse {

    private String status;
    private String key;
    @XmlElement(name = "memberid")
    private Long memberId;
    @XmlElement(name = "teamid")
    private Long teamId;
    private String reason;
    @XmlElements({
        @XmlElement(name = "member", type = Member.class),
        @XmlElement(name = "team", type = Team.class),
        @XmlElement(name = "player", type = Player.class),
        @XmlElement(name = "fixture", type = Fixture.class),
        @XmlElement(name = "livescoring", type = MatchScore.class),
        @XmlElement(name = "player_statistics", type = PlayerStatistics.class),
        @XmlElement(name = "entry", type = PlayerHistory.class),
        @XmlElement(name = "event", type = MatchEvent.class),
        @XmlElement(name = "lineup", type = Lineup.class),
        @XmlElement(name = "standing", type = Standing.class),
        @XmlElement(name = "team_statistics", type = TeamStatistics.class)
    })
    private List<AbstractBlackoutEntity> entities;

    public final String getStatus() {
        return status;
    }

    public final String getKey() {
        return key;
    }

    public final Long getMemberId() {
        return memberId;
    }

    public final Long getTeamId() {
        return teamId;
    }

    public final String getReason() {
        return reason;
    }

    public List<AbstractBlackoutEntity> getEntities() {
        return entities;
    }
}
