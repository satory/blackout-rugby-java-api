/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "lineup")
@XmlAccessorType(XmlAccessType.FIELD)
public class Lineup extends AbstractBlackoutEntity {
    private Long id;
    @XmlElement(name = "teamid")
    private Long teamId;
    @XmlElement(name = "fixtureid")
    private Long fixtureId;
    private Long p1;
    private Long p2;
    private Long p3;
    private Long p4;
    private Long p5;
    private Long p6;
    private Long p7;
    private Long p8;
    private Long p9;
    private Long p10;
    private Long p11;
    private Long p12;
    private Long p13;
    private Long p14;
    private Long p15;
    
    private Long b1;
    private Long b2;
    private Long b3;
    private Long b4;
    private Long b5;
    private Long b6;
    private Long b7;
    
    private Long captain;
    private Long kicker;
    private Long deadline;
    
    private Long r1p1;
    private Long r1p2;
    private Long r1p3;
    private Long r1p4;
    private Long r1p5;
    private Long r1p6;
    private Long r1p7;
    private Long r1p8;
    private Long r1p9;
    private Long r1p10;
    private Long r1p11;
    private Long r1p12;
    private Long r1p13;
    private Long r1p14;
    private Long r1p15;
    
    private Long r2p1;
    private Long r2p2;
    private Long r2p3;
    private Long r2p4;
    private Long r2p5;
    private Long r2p6;
    private Long r2p7;
    private Long r2p8;
    private Long r2p9;
    private Long r2p10;
    private Long r2p11;
    private Long r2p12;
    private Long r2p13;
    private Long r2p14;
    private Long r2p15;

    private Long kicker1;
    private Long kicker2;
    private Long captain1;
    private Long captain2;
    
    @XmlElement(name = "pickandgo")
    private Integer pickAndGo;
    private Integer driving;
    private Integer expansive;
    private Integer creative;
    private String defense;
    private Integer kicking;
    @XmlElement(name = "kickfortouch")
    private Integer kickForTouch;
    @XmlElement(name = "upandunder")
    private Integer upAndUnder;
    @XmlElement(name = "dropgoals")
    private Integer dropGoals;
    private Integer intensity;
    private Integer discipline;
    
    public Long getId() {
        return id;
    }

    public Long getTeamId() {
        return teamId;
    }

    public Long getFixtureId() {
        return fixtureId;
    }

    public Long getP1() {
        return p1;
    }

    public Long getP2() {
        return p2;
    }

    public Long getP3() {
        return p3;
    }

    public Long getP4() {
        return p4;
    }

    public Long getP5() {
        return p5;
    }

    public Long getP6() {
        return p6;
    }

    public Long getP7() {
        return p7;
    }

    public Long getP8() {
        return p8;
    }

    public Long getP9() {
        return p9;
    }

    public Long getP10() {
        return p10;
    }

    public Long getP11() {
        return p11;
    }

    public Long getP12() {
        return p12;
    }

    public Long getP13() {
        return p13;
    }

    public Long getP14() {
        return p14;
    }

    public Long getP15() {
        return p15;
    }

    public Long getB1() {
        return b1;
    }

    public Long getB2() {
        return b2;
    }

    public Long getB3() {
        return b3;
    }

    public Long getB4() {
        return b4;
    }

    public Long getB5() {
        return b5;
    }

    public Long getB6() {
        return b6;
    }

    public Long getB7() {
        return b7;
    }

    public Long getCaptain() {
        return captain;
    }

    public Long getKicker() {
        return kicker;
    }

    public Long getDeadline() {
        return deadline;
    }

    public Long getR1p1() {
        return r1p1;
    }

    public Long getR1p2() {
        return r1p2;
    }

    public Long getR1p3() {
        return r1p3;
    }

    public Long getR1p4() {
        return r1p4;
    }

    public Long getR1p5() {
        return r1p5;
    }

    public Long getR1p6() {
        return r1p6;
    }

    public Long getR1p7() {
        return r1p7;
    }

    public Long getR1p8() {
        return r1p8;
    }

    public Long getR1p9() {
        return r1p9;
    }

    public Long getR1p10() {
        return r1p10;
    }

    public Long getR1p11() {
        return r1p11;
    }

    public Long getR1p12() {
        return r1p12;
    }

    public Long getR1p13() {
        return r1p13;
    }

    public Long getR1p14() {
        return r1p14;
    }

    public Long getR1p15() {
        return r1p15;
    }

    public Long getR2p1() {
        return r2p1;
    }

    public Long getR2p2() {
        return r2p2;
    }

    public Long getR2p3() {
        return r2p3;
    }

    public Long getR2p4() {
        return r2p4;
    }

    public Long getR2p5() {
        return r2p5;
    }

    public Long getR2p6() {
        return r2p6;
    }

    public Long getR2p7() {
        return r2p7;
    }

    public Long getR2p8() {
        return r2p8;
    }

    public Long getR2p9() {
        return r2p9;
    }

    public Long getR2p10() {
        return r2p10;
    }

    public Long getR2p11() {
        return r2p11;
    }

    public Long getR2p12() {
        return r2p12;
    }

    public Long getR2p13() {
        return r2p13;
    }

    public Long getR2p14() {
        return r2p14;
    }

    public Long getR2p15() {
        return r2p15;
    }

    public Long getKicker1() {
        return kicker1;
    }

    public Long getKicker2() {
        return kicker2;
    }

    public Long getCaptain1() {
        return captain1;
    }

    public Long getCaptain2() {
        return captain2;
    }

    public Integer getPickAndGo() {
        return pickAndGo;
    }

    public Integer getDriving() {
        return driving;
    }

    public Integer getExpansive() {
        return expansive;
    }

    public Integer getCreative() {
        return creative;
    }

    public String getDefense() {
        return defense;
    }

    public Integer getKicking() {
        return kicking;
    }

    public Integer getKickForTouch() {
        return kickForTouch;
    }

    public Integer getUpAndUnder() {
        return upAndUnder;
    }

    public Integer getDropGoals() {
        return dropGoals;
    }

    public Integer getIntensity() {
        return intensity;
    }

    public Integer getDiscipline() {
        return discipline;
    }
}