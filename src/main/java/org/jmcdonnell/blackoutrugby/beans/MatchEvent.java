/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "event")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchEvent extends AbstractBlackoutEntity {

    private Long id;
    @XmlElement(name = "fixtureid")
    private Long fixtureId;
    private String commentary;
    private Integer possession;
    @XmlElement(name = "actualtime")
    private Integer actualTime;
    @XmlElement(name = "losttime")
    private Integer lostTime;
    @XmlElement(name = "gametime")
    private Integer gameTime;
    @XmlElement(name = "homescore")
    private Integer homeScore;
    @XmlElement(name = "guestscore")
    private Integer guestScore;
    private Integer index;
    private Integer half;

    public Long getId() {
        return id;
    }

    public Long getFixtureId() {
        return fixtureId;
    }

    public String getCommentary() {
        return commentary;
    }

    public Integer getPossession() {
        return possession;
    }

    public Integer getActualTime() {
        return actualTime;
    }

    public Integer getLostTime() {
        return lostTime;
    }

    public Integer getGameTime() {
        return gameTime;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public Integer getGuestScore() {
        return guestScore;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getHalf() {
        return half;
    }
}
