/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 *
 * @author john
 */
public class TeamTrainingPlayer {
    private Long id;
    @XmlElementWrapper(name = "drops")
    @XmlElement(name = "skill")
    private List<String> drops;
    @XmlElementWrapper(name = "pops")
    @XmlElement(name = "skill")
    private List<String> pops;
    
//    <csr>
//      <old>10185</old>
//      <new>10205</new>
//    </csr>
}
