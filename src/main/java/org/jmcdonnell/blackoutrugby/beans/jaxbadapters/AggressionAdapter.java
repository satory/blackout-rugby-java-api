/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.jaxbadapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.jmcdonnell.blackoutrugby.beans.enums.AggressionEnum;

/**
 *
 * @author john
 */
public class AggressionAdapter extends XmlAdapter<Integer, AggressionEnum> {

    @Override
    public AggressionEnum unmarshal(Integer v) throws Exception {
        return v == null ? null : AggressionEnum.values()[v - 1];
    }

    @Override
    public Integer marshal(AggressionEnum v) throws Exception {
        return (v.ordinal() - 1);
    }   
}