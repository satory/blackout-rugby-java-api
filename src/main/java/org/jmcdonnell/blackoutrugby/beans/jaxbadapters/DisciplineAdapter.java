/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.jaxbadapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.jmcdonnell.blackoutrugby.beans.enums.DisciplineEnum;

/**
 *
 * @author john
 */
public class DisciplineAdapter extends XmlAdapter<Integer, DisciplineEnum> {

    @Override
    public DisciplineEnum unmarshal(Integer v) throws Exception {
        return v == null ? null : DisciplineEnum.values()[v - 1];
    }

    @Override
    public Integer marshal(DisciplineEnum v) throws Exception {
        return (v.ordinal() - 1);
    }
}