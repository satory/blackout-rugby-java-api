/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.jaxbadapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.jmcdonnell.blackoutrugby.beans.enums.PlayerSkillsEnum;

/**
 *
 * @author john
 */
public class PlayerSkillsAdapter extends XmlAdapter<Integer, PlayerSkillsEnum> {

    @Override
    public PlayerSkillsEnum unmarshal(Integer v) throws Exception {
        return v == null ? null : PlayerSkillsEnum.values()[v - 1];
    }

    @Override
    public Integer marshal(PlayerSkillsEnum v) throws Exception {
        return (v.ordinal() - 1);
    }
    
}
