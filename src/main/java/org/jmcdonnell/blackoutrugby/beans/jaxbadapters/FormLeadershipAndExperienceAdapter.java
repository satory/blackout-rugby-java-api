/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.jaxbadapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.jmcdonnell.blackoutrugby.beans.enums.FormLeadershipAndExperienceEnum;

/**
 *
 * @author john
 */
public class FormLeadershipAndExperienceAdapter extends XmlAdapter<Integer, FormLeadershipAndExperienceEnum> {

    @Override
    public FormLeadershipAndExperienceEnum unmarshal(Integer v) throws Exception {
        return v == null ? null : FormLeadershipAndExperienceEnum.values()[v - 1];
    }

    @Override
    public Integer marshal(FormLeadershipAndExperienceEnum v) throws Exception {
        return (v.ordinal() - 1);
    }
    
    
}
