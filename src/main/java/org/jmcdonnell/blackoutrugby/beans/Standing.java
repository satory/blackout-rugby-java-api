/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "standing")
@XmlAccessorType(XmlAccessType.FIELD)
public class Standing extends AbstractBlackoutEntity {
    private Long id;
    @XmlElement(name = "country_iso")
    private String countryIso;
    private Integer season;
    private Integer division;
    private Integer league;
    private Integer standing;
    @XmlElement(name = "teamid")
    private Long teamId;
    @XmlElement(name = "leagueid")
    private Long leagueId;
    private Integer played;
    @XmlElement(name = "w")
    private Integer won;
    @XmlElement(name = "l")
    private Integer lost;
    @XmlElement(name = "d")
    private Integer drawn;
    @XmlElement(name = "for")
    private Integer pointsFor;
    @XmlElement(name = "against")
    private Integer pointsAgainst;
    private Integer b1;
    private Integer b2;
    private Integer points;

    public Long getId() {
        return id;
    }

    public String getCountryIso() {
        return countryIso;
    }

    public Integer getSeason() {
        return season;
    }

    public Integer getDivision() {
        return division;
    }

    public Integer getLeague() {
        return league;
    }

    public Integer getStanding() {
        return standing;
    }

    public Long getTeamId() {
        return teamId;
    }

    public Long getLeagueId() {
        return leagueId;
    }

    public Integer getPlayed() {
        return played;
    }

    public Integer getWon() {
        return won;
    }

    public Integer getLost() {
        return lost;
    }

    public Integer getDrawn() {
        return drawn;
    }

    public Integer getPointsFor() {
        return pointsFor;
    }

    public Integer getPointsAgainst() {
        return pointsAgainst;
    }

    public Integer getB1() {
        return b1;
    }

    public Integer getB2() {
        return b2;
    }

    public Integer getPoints() {
        return points;
    }
}
