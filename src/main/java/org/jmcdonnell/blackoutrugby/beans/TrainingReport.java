/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author john
 */
@XmlRootElement(name = "training_report")
@XmlAccessorType(XmlAccessType.FIELD)
public class TrainingReport {
    private Long id;
    @XmlElement(name = "teamid")
    private Long teamId;
    private Integer season;
    private Integer round;
    @XmlElement(name = "team_training")
    private TeamTraining teamTraining;
    @XmlElementWrapper(name = "individual_training")
    @XmlElement(name = "player")
    private List<IndividualPlayerTraining> playerTraining;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public TeamTraining getTeamTraining() {
        return teamTraining;
    }

    public void setTeamTraining(TeamTraining teamTraining) {
        this.teamTraining = teamTraining;
    }

    public List<IndividualPlayerTraining> getPlayerTraining() {
        return playerTraining;
    }

    public void setPlayerTraining(List<IndividualPlayerTraining> playerTraining) {
        this.playerTraining = playerTraining;
    }
}
