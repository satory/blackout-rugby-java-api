/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.jmcdonnell.blackoutrugby.beans.enums.AggressionEnum;
import org.jmcdonnell.blackoutrugby.beans.enums.DisciplineEnum;
import org.jmcdonnell.blackoutrugby.beans.enums.FormLeadershipAndExperienceEnum;
import org.jmcdonnell.blackoutrugby.beans.enums.PlayerSkillsEnum;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.AggressionAdapter;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.DisciplineAdapter;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.FormLeadershipAndExperienceAdapter;
import org.jmcdonnell.blackoutrugby.beans.jaxbadapters.PlayerSkillsAdapter;

/**
 *
 * @author john
 */
@XmlRootElement(name = "player")
@XmlAccessorType(XmlAccessType.FIELD)
public class Player extends AbstractBlackoutEntity {
    @XmlElement(name = "id")
    private Long playerId;
    @XmlElement(name = "teamid")
    private Long teamId;
    @XmlElement(name = "fname")
    private String fName;
    @XmlElement(name = "lname")
    private String lName;
    @XmlElement(name = "birthday")
    private String birthday;
    @XmlElement(name = "age")
    private Integer age;
    @XmlElement(name = "hand")
    private String hand;
    @XmlElement(name = "foot")
    private String foot;
    @XmlElement(name = "nationality")
    private String nationality;
    @XmlElement(name = "csr")
    private Long csr;
    private Long prevCsr;
    @XmlElement(name = "salary")
    private Long salary;
    @XmlElement(name = "form")
    @XmlJavaTypeAdapter(FormLeadershipAndExperienceAdapter.class)
    private FormLeadershipAndExperienceEnum form;
    @XmlElement(name = "aggression")
    @XmlJavaTypeAdapter(AggressionAdapter.class)
    private AggressionEnum aggression;
    @XmlElement(name = "discipline")
    @XmlJavaTypeAdapter(DisciplineAdapter.class)
    private DisciplineEnum discipline;
    @XmlElement(name = "energy")
    private Integer energy;
    @XmlElement(name = "leadership")
    @XmlJavaTypeAdapter(FormLeadershipAndExperienceAdapter.class)
    private FormLeadershipAndExperienceEnum leadership;
    @XmlElement(name = "experience")
    @XmlJavaTypeAdapter(FormLeadershipAndExperienceAdapter.class)
    private FormLeadershipAndExperienceEnum experience;
    @XmlElement(name = "weight")
    private Integer weight;
    @XmlElement(name = "height")
    private Integer height;
    @XmlElement(name = "injured")
    private Long injured;
    @XmlElement(name = "youthid")
    private Long youthId;
    @XmlElement(name = "jersey")
    private Integer jersey;
    @XmlElement(name = "joined")
    private Long joined;
    @XmlElement(name = "rawsalary")
    private Long rawSalary;
    @XmlElement(name = "stamina")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum stamina;
    @XmlElement(name = "contract")
    private Long contract;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "dualnationality")
    private String dualNationality;
    @XmlElement(name = "kicking")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum kicking;
    @XmlElement(name = "agility")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum agility;
    @XmlElement(name = "speed")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum speed;
    @XmlElement(name = "jumping")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum jumping;
    @XmlElement(name = "strength")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum strength;
    @XmlElement(name = "technique")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum technique;
    @XmlElement(name = "defense")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum defense;
    @XmlElement(name = "attack")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum attack;
    @XmlElement(name = "handling")
    @XmlJavaTypeAdapter(PlayerSkillsAdapter.class)
    private PlayerSkillsEnum handling;
    @XmlElementWrapper(name = "pops")
    @XmlElement(name = "skill")
    private List<String>pops;

    /**
     *
     * {@inheritDoc}
     */
    public Integer getAge() {
        return age;
    }

    /**
     * {@inheritDoc}
     */
    public AggressionEnum getAggression() {
        return aggression;
    }

    /**
     * {@inheritDoc}
     */
    public String getBirthday() {
        return birthday;
    }
    
    /**
     * {@inheritDoc}
     */
    public Long getCsr() {
        return csr;
    }

    /**
     * {@inheritDoc}
     */
    public Long getPrevCsr() {
        return prevCsr;
    }

    /**
     * {@inheritDoc}
     */
    public DisciplineEnum getDiscipline() {
        return discipline;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getEnergy() {
        return energy;
    }

    /**
     * {@inheritDoc}
     */
    public FormLeadershipAndExperienceEnum getExperience() {
        return experience;
    }

    /**
     * {@inheritDoc}
     */
    public String getfName() {
        return fName;
    }

    /**
     * {@inheritDoc}
     */
    public FormLeadershipAndExperienceEnum getForm() {
        return form;
    }

    /**
     * {@inheritDoc}
     */
    public String getHand() {
        return hand;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * {@inheritDoc}
     */
    public Long getInjured() {
        return injured;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getJersey() {
        return jersey;
    }

    /**
     * {@inheritDoc}
     */
    public Long getJoined() {
        return joined;
    }

    /**
     * {@inheritDoc}
     */
    public String getlName() {
        return lName;
    }

    /**
     * {@inheritDoc}
     */
    public FormLeadershipAndExperienceEnum getLeadership() {
        return leadership;
    }

    /**
     * {@inheritDoc}
     */
    public String getFoot() {
        return foot;
    }

    /**
     * {@inheritDoc}
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * {@inheritDoc}
     */
    public Long getPlayerId() {
        return playerId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getSalary() {
        return salary;
    }

    /**
     * {@inheritDoc}
     */
    public Long getTeamId() {
        return teamId;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * {@inheritDoc}
     */
    public Long getYouthId() {
        return youthId;
    }

    /**
     * {@inheritDoc}
     */
    public Long getRawSalary() {
        return rawSalary;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getStamina() {
        return stamina;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getHandling() {
        return handling;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getAttack() {
        return attack;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getDefense() {
        return defense;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getTechnique() {
        return technique;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getStrength() {
        return strength;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getJumping() {
        return jumping;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getSpeed() {
        return speed;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getAgility() {
        return agility;
    }

    /**
     * {@inheritDoc}
     */
    public PlayerSkillsEnum getKicking() {
        return kicking;
    }

    /**
     * {@inheritDoc}
     */
    public String getDualNationality() {
        return dualNationality;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }
    
    /**
     * {@inheritDoc}
     */
    public Long getContract() {
        return contract;
    }

    public List<String> getPops() {
        return pops;
    }
}
