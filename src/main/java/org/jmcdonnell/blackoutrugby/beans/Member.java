/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author john
 */
@XmlRootElement(name = "member")
@XmlAccessorType(XmlAccessType.FIELD)
public class Member extends AbstractBlackoutEntity {

    @XmlElement(name = "id")
    private Long memberId;
    @XmlElement(name = "username")
    private String userName;
    @XmlElement(name = "realname")
    private String realName;
    @XmlElement(name = "email")
    private String email;
    @XmlElement(name = "memberlevel")
    private Integer memberLevel;
    @XmlElement(name = "teamid")
    private Long teamId;
    @XmlElement(name = "cash")
    private Double cash;
    @XmlElement(name = "active")
    private Integer active;
    @XmlElement(name = "renames")
    private Integer renames;
    @XmlElement(name = "moves")
    private Integer moves;
    @XmlElement(name = "surveyed")
    private Integer surveyed;
    @XmlElement(name = "regmethod")
    private Integer regMethod;
    @XmlElement(name = "team_country_iso")
    private String teamCountryIso;
    @XmlElement(name = "main_teamid")
    private Long mainTeamId;
    @XmlElement(name = "lastclick")
    private Long lastClick;
    @XmlElement(name = "dateregistered")
    private Long dateRegistered;
    @XmlElementWrapper(name="teams")
    @XmlElement(name = "teamid")
    private List<Long> teams;
    
    /**
     * {@inheritDoc}
     */
    public Integer getActive() {
        return active;
    }

    /**
     * {@inheritDoc}
     */
    public Double getCash() {
        return cash;
    }

    /**
     * {@inheritDoc}
     */
    public Long getDateRegistered() {
        return dateRegistered;
    }

    /**
     * {@inheritDoc}
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * {@inheritDoc}
     */
    public Long getLastClick() {
        return lastClick;
    }

    /**
     * {@inheritDoc}
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getMemberLevel() {
        return memberLevel;
    }

    /**
     * {@inheritDoc}
     */
    public String getRealName() {
        return realName;
    }

    /**
     * {@inheritDoc}
     */
    public Long getTeamId() {
        return teamId;
    }

    /**
     * {@inheritDoc}
     */
    public String getUserName() {
        return userName;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getMoves() {
        return moves;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getRenames() {
        return renames;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getSurveyed() {
        return surveyed;
    }

    public Integer getRegMethod() {
        return regMethod;
    }

    public String getTeamCountryIso() {
        return teamCountryIso;
    }

    public Long getMainTeamId() {
        return mainTeamId;
    }

    public List<Long> getTeams() {
        return teams;
    }
}