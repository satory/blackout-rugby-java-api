/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.enums;

/**
 *
 * @author john
 */
public enum DisciplineEnum {
    REBELLIOUS("Rebellious"),
    RECKLESS("Reckless"),
    COLLECTED("Collected"),
    CONTROLLED("Controlled"),
    FLAWLESS("Flawless");
    
    private String name;

    private DisciplineEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
