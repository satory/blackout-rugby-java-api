/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.enums;

/**
 *
 * @author john
 */
public enum PlayerSkillsEnum {
    NON_EXISTENT("Non-Existent"),
    DESPICABLE("Despicable"),
    HORRIBLE("Horrible"),
    LIMITED("Limited"),
    MODERATE("Moderate"),
    AVERAGE("Average"),
    SATISFACTORY("Satisfactory"),
    DECENT("Decent"),
    REPUTABLE("Reputable"),
    ADMIRABLE("Admirable"),
    IMPRESSIVE("Impressive"),
    PRINCELY("Princely"),
    SUMPTUOUS("Sumptuous"),
    AMAZING("Amazing"),
    GRAND("Grand"),
    SUPERIOR("Superior"),
    MARVELOUS("Marvelous"),
    MONUMENTAL("Monumental"),
    MAJESTIC("Majestic"),
    LEGENDARY("Legendary"),
    GODLY("Godly");
    
    private String name;

    private PlayerSkillsEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
