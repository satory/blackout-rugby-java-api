/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.enums;

/**
 *
 * @author john
 */
public enum AggressionEnum {
    TIMID("Timid"),
    CONSERVATIVE("Conservative"),
    COLLECTED("Collected"),
    AGGRESSIVE("Aggressive"),
    PSYCHOTIC("Psychotic");
    
    private String name;

    private AggressionEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
