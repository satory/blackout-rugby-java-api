/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans.enums;

/**
 *
 * @author john
 */
public enum FormLeadershipAndExperienceEnum {
    NON_EXISTENT("Non-Existent"),
    DESPICABLE("Despicable"),
    HORRIBLE("Horrible"),
    LIMITED("Limited"),
    MODERATE("Moderate"),
    AVERAGE("Average"),
    SATISFACTORY("Satisfactory"),
    DECENT("Decent"),
    REPUTABLE("Reputable"),
    ADMIRABLE("Admirable"),
    IMPRESSIVE("Impressive");
    
    private String name;

    private FormLeadershipAndExperienceEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
