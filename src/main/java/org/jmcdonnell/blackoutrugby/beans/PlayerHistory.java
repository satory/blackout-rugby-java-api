/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "entry")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerHistory extends AbstractBlackoutEntity {
    
    @XmlElement(name = "playerid")
    private Long playerId;
    private String event;
    private Long date;
    
    public Long getPlayerId() {
        return playerId;
    }

    public String getEvent() {
        return event;
    }

    public Long getDate() {
        return date;
    }
}
