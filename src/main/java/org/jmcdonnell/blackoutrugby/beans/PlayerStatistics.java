/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "player_statistics")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerStatistics extends AbstractBlackoutEntity {
    
    @XmlElement(name = "playerid")
    private Long playerId;
    private Integer tackles;
    @XmlElement(name = "metresgained")
    private Integer metresGained;
    private Integer tries;
    private Integer conversions;
    @XmlElement(name = "dropgoals") 
    private Integer dropGoals;
    private Integer penalties;
    @XmlElement(name = "totalpoints")
    private Integer totalPoints;
    @XmlElement(name = "yellowcards")
    private Integer yellowCards;
    @XmlElement(name = "redcards") 
    private Integer redCards;
    @XmlElement(name = "linebreaks")
    private Integer lineBreaks;
    private Integer intercepts;
    private Integer kicks;
    @XmlElement(name = "knockons")
    private Integer knockOns;
    @XmlElement(name = "forwardpasses")
    private Integer forwardPasses;
    @XmlElement(name = "tryassists")
    private Integer tryAssists;
    @XmlElement(name = "beatendefenders")
    private Integer beatenDefenders;
    private Integer injuries;
    @XmlElement(name = "handlingerrors")
    private Integer handlingErrors;
    @XmlElement(name = "missedtackles")
    private Integer missedTackles;
    private Integer fights;
    @XmlElement(name = "kickingmetres")
    private Integer kickingMetres;
    @XmlElement(name = "leaguecaps")
    private Integer leagueCaps;
    @XmlElement(name = "friendlycaps")
    private Integer friendlyCaps;
    @XmlElement(name = "cupcaps")
    private Integer cupCaps;
    @XmlElement(name = "undertwentycaps")
    private Integer under20Caps;
    @XmlElement(name = "nationalcaps")
    private Integer nationalCaps;
    @XmlElement(name = "othercaps") 
    private Integer otherCaps;
    @XmlElement(name = "avkickingmetres")
    private Double averageKickingMetres;
    @XmlElement(name = "penaltiesconceded")
    private Integer penaltiesConceded;
    @XmlElement(name= "kicksoutonthefull")
    private Integer kicksOutOnTheFull;
    @XmlElement(name = "balltime")
    private Integer ballTime;
    @XmlElement(name = "penaltytime")
    private Integer penaltyTime;
    @XmlElement(name = "missedconversions")
    private Integer missedConversions;
    @XmlElement(name = "misseddropgoals")
    private Integer missedDropGoals;
    @XmlElement(name = "missedpenalties")
    private Integer missedPenalties;
    @XmlElement(name = "goodupandunders")
    private Integer goodUpAndUnders;
    @XmlElement(name = "badupandunders")
    private Integer badUpAndUnders;
    @XmlElement(name = "upandunders")
    private Integer upAndUnders;
    @XmlElement(name = "goodkicks")
    private Integer goodKicks;
    @XmlElement(name = "badkicks")
    private Integer badKicks;
    @XmlElement(name = "turnoverswon")
    private Integer turnOversWon;
    @XmlElement(name = "lineoutssecured")
    private Integer lineoutsSecured;
    @XmlElement(name = "lineoutsconceded")
    private Integer lineoutsConceeded;
    @XmlElement(name = "lineoutsstolen")
    private Integer lineoutsStolen;
    @XmlElement(name = "successfullineoutthrows")
    private Integer successfulLineoutThrows;
    @XmlElement(name = "unsuccessfullineoutthrows")
    private Integer unsuccessfulLineoutThrows;
    @XmlElement(name = "worldcupcaps")
    private Integer worldCupCaps;
    @XmlElement(name = "undertwentyworldcupcaps")
    private Integer under20WorldCupCaps;

    public Long getPlayerId() {
        return playerId;
    }

    public Integer getTackles() {
        return tackles;
    }

    public Integer getMetresGained() {
        return metresGained;
    }

    public Integer getTries() {
        return tries;
    }

    public Integer getConversions() {
        return conversions;
    }

    public Integer getDropGoals() {
        return dropGoals;
    }

    public Integer getPenalties() {
        return penalties;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public Integer getYellowCards() {
        return yellowCards;
    }

    public Integer getRedCards() {
        return redCards;
    }

    public Integer getLineBreaks() {
        return lineBreaks;
    }

    public Integer getIntercepts() {
        return intercepts;
    }

    public Integer getKicks() {
        return kicks;
    }

    public Integer getKnockOns() {
        return knockOns;
    }

    public Integer getForwardPasses() {
        return forwardPasses;
    }

    public Integer getTryAssists() {
        return tryAssists;
    }

    public Integer getBeatenDefenders() {
        return beatenDefenders;
    }

    public Integer getInjuries() {
        return injuries;
    }

    public Integer getHandlingErrors() {
        return handlingErrors;
    }

    public Integer getMissedTackles() {
        return missedTackles;
    }

    public Integer getFights() {
        return fights;
    }

    public Integer getKickingMetres() {
        return kickingMetres;
    }

    public Integer getLeagueCaps() {
        return leagueCaps;
    }

    public Integer getFriendlyCaps() {
        return friendlyCaps;
    }

    public Integer getCupCaps() {
        return cupCaps;
    }

    public Integer getUnder20Caps() {
        return under20Caps;
    }

    public Integer getNationalCaps() {
        return nationalCaps;
    }

    public Integer getOtherCaps() {
        return otherCaps;
    }

    public Double getAverageKickingMetres() {
        return averageKickingMetres;
    }

    public Integer getPenaltiesConceded() {
        return penaltiesConceded;
    }

    public Integer getKicksOutOnTheFull() {
        return kicksOutOnTheFull;
    }

    public Integer getBallTime() {
        return ballTime;
    }

    public Integer getPenaltyTime() {
        return penaltyTime;
    }

    public Integer getMissedConversions() {
        return missedConversions;
    }

    public Integer getMissedDropGoals() {
        return missedDropGoals;
    }

    public Integer getMissedPenalties() {
        return missedPenalties;
    }

    public Integer getGoodUpAndUnders() {
        return goodUpAndUnders;
    }

    public Integer getBadUpAndUnders() {
        return badUpAndUnders;
    }

    public Integer getUpAndUnders() {
        return upAndUnders;
    }

    public Integer getGoodKicks() {
        return goodKicks;
    }

    public Integer getBadKicks() {
        return badKicks;
    }

    public Integer getTurnOversWon() {
        return turnOversWon;
    }

    public Integer getLineoutsSecured() {
        return lineoutsSecured;
    }

    public Integer getLineoutsConceeded() {
        return lineoutsConceeded;
    }

    public Integer getLineoutsStolen() {
        return lineoutsStolen;
    }

    public Integer getSuccessfulLineoutThrows() {
        return successfulLineoutThrows;
    }

    public Integer getUnsuccessfulLineoutThrows() {
        return unsuccessfulLineoutThrows;
    }

    public Integer getWorldCupCaps() {
        return worldCupCaps;
    }

    public Integer getUnder20WorldCupCaps() {
        return under20WorldCupCaps;
    }
}