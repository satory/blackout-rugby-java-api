/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.lineups;

import org.jmcdonnell.blackoutrugby.beans.Lineup;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface ILineupService {
    
    Lineup getDefaultLineupForTeam(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    Lineup getLineUpForTeamFromFixture(Long teamId, Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}
