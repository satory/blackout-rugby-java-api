package org.jmcdonnell.blackoutrugby.services.lineups;

import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Lineup;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "lineupService")
public class ILineupServiceImpl implements ILineupService {

    @Inject private RequestManager requestManager;
    
    @Override
    public Lineup getDefaultLineupForTeam(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(teamId, Lineup.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public Lineup getLineUpForTeamFromFixture(Long teamId, Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getLineupForTeamAndFixture(teamId, fixtureId, isNationalTeam, isYouthTeam);
    }
}