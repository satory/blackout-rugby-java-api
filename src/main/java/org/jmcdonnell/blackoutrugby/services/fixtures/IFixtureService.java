/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.fixtures;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Fixture;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface IFixtureService {
     Fixture getFixtureById(Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
     List<Fixture> getFixturesByIds(List<Long> fixtureIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}
