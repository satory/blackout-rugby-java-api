package org.jmcdonnell.blackoutrugby.services.fixtures;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Fixture;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "fixtureService")
public class IFixtureServiceImpl implements IFixtureService {

    @Inject private RequestManager requestManager;
    
    @Override
    public Fixture getFixtureById(Long fixtureId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(fixtureId, Fixture.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Fixture> getFixturesByIds(List<Long> fixtureIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntitiesFromApi(fixtureIds, Fixture.class, isNationalTeam, isYouthTeam);
    }
}