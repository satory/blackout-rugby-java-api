/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.statistics;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.PlayerStatistics;
import org.jmcdonnell.blackoutrugby.beans.TeamStatistics;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface IStatisticsService {
    
    PlayerStatistics getPlayerStatisticsByPlayerId(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    List<PlayerStatistics> getPlayerStatisticsByPlayerIds(List<Long>playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    TeamStatistics getTeamStatisticsByTeamId(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}
