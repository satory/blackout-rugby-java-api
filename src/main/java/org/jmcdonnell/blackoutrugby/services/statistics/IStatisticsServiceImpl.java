package org.jmcdonnell.blackoutrugby.services.statistics;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.PlayerStatistics;
import org.jmcdonnell.blackoutrugby.beans.TeamStatistics;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "statisticsService")
public class IStatisticsServiceImpl implements IStatisticsService {

    @Inject private RequestManager requestManager;
    
    @Override
    public PlayerStatistics getPlayerStatisticsByPlayerId(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(playerId, PlayerStatistics.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<PlayerStatistics> getPlayerStatisticsByPlayerIds(List<Long> playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntitiesFromApi(playerIds, PlayerStatistics.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public TeamStatistics getTeamStatisticsByTeamId(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(teamId, TeamStatistics.class, isNationalTeam, isYouthTeam);
    }

}
