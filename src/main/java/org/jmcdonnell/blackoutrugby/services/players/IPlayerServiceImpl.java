package org.jmcdonnell.blackoutrugby.services.players;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Player;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "playerService")
public class IPlayerServiceImpl implements IPlayerService {

    @Inject private RequestManager requestManager;
    
    @Override
    public Player getPlayerById(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(playerId, Player.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Player> getPlayersByIds(List<Long> playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntitiesFromApi(playerIds, Player.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Player> getPlayersInTeamSquad(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getPlayersByTeamId(teamId, isNationalTeam, isYouthTeam);
    }
}