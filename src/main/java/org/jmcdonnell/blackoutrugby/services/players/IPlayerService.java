/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.players;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Player;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface IPlayerService {
    
    Player getPlayerById(Long playerId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    List<Player> getPlayersByIds(List<Long> playerIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    List<Player> getPlayersInTeamSquad(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}