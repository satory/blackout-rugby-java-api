/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.team;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Team;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface ITeamService {
    
    Team getTeamById(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
    
    List<Team> getTeamsByIds(List<Long> teamIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}