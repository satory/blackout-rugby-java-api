package org.jmcdonnell.blackoutrugby.services.team;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Team;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "teamService")
public class ITeamServiceImpl implements ITeamService {

    @Inject private RequestManager requestManager;
    
    @Override
    public Team getTeamById(Long teamId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntityFromApi(teamId, Team.class, isNationalTeam, isYouthTeam);
    }

    @Override
    public List<Team> getTeamsByIds(List<Long> teamIds, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getEntitiesFromApi(teamIds, Team.class, isNationalTeam, isYouthTeam);
    }
}