package org.jmcdonnell.blackoutrugby.services.members;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Member;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "memberService")
public class IMemberServiceImpl implements IMemberService {

    @Inject private RequestManager requestManager;
    
    @Override
    public Member getMemberById(Long memberId) throws BlackoutException {
        return requestManager.getEntityFromApi(memberId, Member.class, Boolean.FALSE, Boolean.FALSE);
    }

    @Override
    public List<Member> getMembersByIds(List<Long> memberIds) throws BlackoutException {
        return requestManager.getEntitiesFromApi(memberIds, Member.class, Boolean.FALSE, Boolean.FALSE);
    }
}