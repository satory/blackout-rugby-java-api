/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.members;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Member;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface IMemberService {
    
    Member getMemberById(Long memberId) throws BlackoutException;
    
    List<Member> getMembersByIds(List<Long> memberIds) throws BlackoutException;
}