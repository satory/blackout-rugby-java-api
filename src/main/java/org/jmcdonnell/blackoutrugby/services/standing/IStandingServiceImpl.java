package org.jmcdonnell.blackoutrugby.services.standing;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.jmcdonnell.blackoutrugby.beans.Standing;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.requests.RequestManager;

@Named(value = "standingService")
public class IStandingServiceImpl implements IStandingService {

    @Inject private RequestManager requestManager;
    
    @Override
    public List<Standing> getStandingsForLeagueId(Long leagueId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException {
        return requestManager.getListOfEntityByIdFromApi(leagueId, Standing.class, isNationalTeam, isYouthTeam);
    }
}