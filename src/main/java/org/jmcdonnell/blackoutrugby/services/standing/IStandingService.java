/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.services.standing;

import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Standing;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;

/**
 *
 * @author john
 */
public interface IStandingService {
    
    List<Standing> getStandingsForLeagueId(Long leagueId, Boolean isNationalTeam, Boolean isYouthTeam) throws BlackoutException;
}
