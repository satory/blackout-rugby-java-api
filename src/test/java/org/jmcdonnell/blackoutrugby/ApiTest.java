/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutLoginException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class ApiTest
{
    private final static String CORRECT_USERNAME = System.getProperty("member.username");
    private final static String CORRECT_PASSWORD = System.getProperty("member.password");

    private final static String INCORRECT_USERNAME = "myusername";
    private final static String INCORRECT_PASSWORD = "mypassword";
    
    @Test
    public void testApiInitNotNull() throws BlackoutException
    {
        BlackoutApi api = BlackoutApi.init(CORRECT_USERNAME, CORRECT_PASSWORD);
        assertNotNull(api);
    }
    
    @Test
    public void testApiInitCorrectLoginInformation() throws BlackoutException
    {
        BlackoutApi api = BlackoutApi.init(CORRECT_USERNAME, CORRECT_PASSWORD);
        assertNotNull(api.getMemberId());
        assertNotNull(api.getMemberApiKey());
        assertNotNull(api.getTeamId());
    }
    
    @Test(expected = BlackoutLoginException.class)
    public void testApiInitBadUsername() throws BlackoutException
    {
        BlackoutApi.init(INCORRECT_USERNAME, "");
    }
    
    @Test(expected = BlackoutLoginException.class)
    public void testApiInitBadPassword() throws BlackoutException
    {
        BlackoutApi.init(CORRECT_USERNAME, INCORRECT_PASSWORD);
    }
}