/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.BlackoutApi;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author john
 */
public abstract class AbstractApiRequestTest {
    protected static final String MEMBER_ID_SYS_PROP = "member.id";
    protected static final String TEAM_ID_SYS_PROP = "team.id";
    protected static final String MEMBER_API_KEY_SYS_PROP = "member.api.key";
    
    protected static BlackoutApi api;
    protected RequestManager requestManager;
    
    protected static String memberId = "";
    protected static String memberApiKey = "";
    protected static String teamId = "";

    @BeforeClass
    public static void classSetUp() throws BlackoutException {
        
        memberId = System.getProperty(MEMBER_ID_SYS_PROP);
        memberApiKey = System.getProperty(MEMBER_API_KEY_SYS_PROP);
        teamId = System.getProperty(TEAM_ID_SYS_PROP);
        
        if (memberId.isEmpty() || teamId.isEmpty() || memberApiKey.isEmpty()) {
            api = BlackoutApi.init(System.getProperty("member.username"), System.getProperty("member.password"));
            
            System.setProperty(MEMBER_ID_SYS_PROP, api.getMemberId().toString());
            System.setProperty(TEAM_ID_SYS_PROP, api.getTeamId().toString());
            System.setProperty(MEMBER_API_KEY_SYS_PROP, api.getMemberApiKey());
        }
        
        System.setProperty(RequestManager.MEMBER_ACCESS_KEY_PROP, "&" + System.getProperty("member.accesskey"));
        System.setProperty("nationalTeamId", "24"); // Poland ID
    }

    @Before
    public void setUp() {
        requestManager = new RequestManager();
    }

    @After
    public void tearDown() {
        requestManager = null;
    }
}