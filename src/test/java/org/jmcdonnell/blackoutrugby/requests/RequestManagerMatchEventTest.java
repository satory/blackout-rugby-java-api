/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.MatchEvent;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerMatchEventTest extends AbstractApiRequestTest {

    @Test
    public void testGetMatchCommentaryForNationalMatchIdNotNull() throws BlackoutException {
        Long fixtureId = new Long(6537);
        List<MatchEvent> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(fixtureId, MatchEvent.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetMatchCommentaryForUnderTwentyMatchIdNotNull() throws BlackoutException {
        Long fixtureId = new Long(3716);
        List<MatchEvent> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(fixtureId, MatchEvent.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetMatchCommentaryForClubMatchIdNotNull() throws BlackoutException {
        Long fixtureId = new Long(System.getProperty("match.saved.id"));
        List<MatchEvent> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(fixtureId, MatchEvent.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetMatchCommentaryForYouthMatchIdNotNull() throws BlackoutException {
        Long fixtureId = new Long(10023659);
        List<MatchEvent> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(fixtureId, MatchEvent.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetMultipleEventsByMatchId() throws BlackoutException {
        Long fixtureId = new Long(System.getProperty("match.saved.id"));
        List<MatchEvent> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(fixtureId, MatchEvent.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(entitiesFromApi.size() > 0);
        MatchEvent get = entitiesFromApi.get(0);

        assertNotNull(get.getCommentary());
        assertTrue(get.getFixtureId().equals(fixtureId));
        assertNotNull(get.getGameTime());
        assertNotNull(get.getGuestScore());
        assertNotNull(get.getHalf());
        assertNotNull(get.getHomeScore());
        assertNotNull(get.getId());
        assertNotNull(get.getIndex());
        assertNotNull(get.getLostTime());
        assertNotNull(get.getPossession());
        assertNotNull(get.getActualTime());
    }
}
