/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.beans.BlackoutLoginDetails;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutLoginException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerLoginTest extends AbstractApiRequestTest {

    private final static String CORRECT_USERNAME = System.getProperty("member.username");
    private final static String CORRECT_PASSWORD = System.getProperty("member.password");

    private final static String INCORRECT_USERNAME = "myusername";
    private final static String INCORRECT_PASSWORD = "mypassword";

    @Test
    public void testCorrectLogin() throws BlackoutException {
        BlackoutLoginDetails login = requestManager.login(CORRECT_USERNAME, CORRECT_PASSWORD);
        assertNotNull(login);
        assertTrue(login.getStatus().equals("Ok"));
    }

    @Test(expected = BlackoutLoginException.class)
    public void testIncorrectUserName() throws BlackoutException {
        requestManager.login(INCORRECT_USERNAME, CORRECT_PASSWORD);
    }

    @Test(expected = BlackoutLoginException.class)
    public void testIncorrectPassword() throws BlackoutException {
        requestManager.login(CORRECT_USERNAME, INCORRECT_PASSWORD);
    }
}
