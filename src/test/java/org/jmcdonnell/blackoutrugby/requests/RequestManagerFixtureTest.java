/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.beans.Fixture;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerFixtureTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalFixtureByIdNotNull() throws BlackoutException {
        Fixture fixture
                = requestManager.getEntityFromApi(new Long(6537), Fixture.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(fixture);
    }

    @Test
    public void testGetUnderTwentyFixtureByIdNotNull() throws BlackoutException {
        Fixture fixture
                = requestManager.getEntityFromApi(new Long(3716), Fixture.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(fixture);
    }

    @Test
    public void testGetClubFixtureByIdNotNull() throws BlackoutException {
        Fixture fixture
                = requestManager.getEntityFromApi(new Long(System.getProperty("fixture.1.id")), Fixture.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(fixture);
    }

    @Test
    public void testGetYouthFixtureByIdNotNull() throws BlackoutException {
        Fixture fixture
                = requestManager.getEntityFromApi(new Long(10023659), Fixture.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(fixture);
    }

    @Test
    public void testGetMultipleFixturesByIdNotNull() throws BlackoutException {
        List<Long> fixtureIds = new ArrayList<Long>();
        fixtureIds.add(new Long(System.getProperty("fixture.1.id")));
        fixtureIds.add(new Long(System.getProperty("fixture.2.id")));
        List<Fixture> entitiesFromApi
                = requestManager.getEntitiesFromApi(fixtureIds, Fixture.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testContentsOfSingleFixtureById() throws BlackoutException {
        Fixture LastFixture = requestManager.getLastFixtureForTeam(new Long(System.getProperty("team.id")), Boolean.FALSE, Boolean.FALSE);

        Fixture fixture
                = requestManager.getEntityFromApi(LastFixture.getFixtureId(), Fixture.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(fixture.getBotMatch());
        assertNotNull(fixture.getCompetition());
        assertNotNull(fixture.getCountryIso());
        assertTrue(fixture.getFixtureId().equals(new Long(System.getProperty("fixture.1.id"))));
        assertNotNull(fixture.getGuestTeamId());
        assertNotNull(fixture.getHomeTeamId());
        assertNotNull(fixture.getLeagueId());
        assertNotNull(fixture.getMatchStart());
        assertNotNull(fixture.getRound());
        assertNotNull(fixture.getSeason());
    }

    @Test
    public void testContentsOfMultipleFixturesById() throws BlackoutException {
        List<Fixture> lastFixtures = requestManager.getLastNumOfFixturesForTeam(
                new Long(System.getProperty("team.id")),
                Boolean.FALSE,
                Boolean.FALSE,
                2);

        List<Long> fixtureIds = new ArrayList<Long>();
        fixtureIds.add(lastFixtures.get(0).getFixtureId());
        fixtureIds.add(lastFixtures.get(1).getFixtureId());
        List<Fixture> entitiesFromApi
                = requestManager.getEntitiesFromApi(fixtureIds, Fixture.class, Boolean.FALSE, Boolean.FALSE);

        Fixture fixture1 = entitiesFromApi.get(0);
        Fixture fixture2 = entitiesFromApi.get(1);

        assertNotNull(fixture1.getBotMatch());
        assertNotNull(fixture1.getCompetition());
        assertNotNull(fixture1.getCountryIso());
        assertTrue(fixture1.getFixtureId().equals(fixtureIds.get(0)));
        assertNotNull(fixture1.getGuestTeamId());
        assertNotNull(fixture1.getHomeTeamId());
        assertNotNull(fixture1.getLeagueId());
        assertNotNull(fixture1.getMatchStart());
        assertNotNull(fixture1.getRound());
        assertNotNull(fixture1.getSeason());

        assertNotNull(fixture2.getBotMatch());
        assertNotNull(fixture2.getCompetition());
        assertNotNull(fixture2.getCountryIso());
        assertTrue(fixture2.getFixtureId().equals(fixtureIds.get(0)));
        assertNotNull(fixture2.getGuestTeamId());
        assertNotNull(fixture2.getHomeTeamId());
        assertNotNull(fixture2.getLeagueId());
        assertNotNull(fixture2.getMatchStart());
        assertNotNull(fixture2.getRound());
        assertNotNull(fixture2.getSeason());
    }
}
