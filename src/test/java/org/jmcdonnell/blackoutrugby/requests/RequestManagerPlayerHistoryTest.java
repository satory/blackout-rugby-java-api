/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.PlayerHistory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerPlayerHistoryTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalPlayerHistoryByIdNotNull() throws BlackoutException {
        Long playerId = new Long(System.getProperty("player.national.id"));
        List<PlayerHistory> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(playerId, PlayerHistory.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetUnderTwentyPlayerHistoryByIdNotNull() throws BlackoutException {
        Long playerId = new Long(System.getProperty("player.undertwenty.id"));
        List<PlayerHistory> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(playerId, PlayerHistory.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetClubPlayerHistoryByIdNotNull() throws BlackoutException {
        Long playerId = new Long(System.getProperty("player.1.id"));
        List<PlayerHistory> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(playerId, PlayerHistory.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetPlayerHistoryById() throws BlackoutException {
        Long playerId = new Long(System.getProperty("player.1.id"));
        List<PlayerHistory> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(playerId, PlayerHistory.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(entitiesFromApi.size() > 0);
        PlayerHistory get = entitiesFromApi.get(0);

        assertNotNull(get.getDate());
        assertNotNull(get.getEvent());
        assertTrue(get.getPlayerId().equals(playerId));
    }
}
