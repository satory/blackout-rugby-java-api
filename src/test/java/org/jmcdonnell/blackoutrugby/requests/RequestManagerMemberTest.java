/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.beans.Member;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerMemberTest extends AbstractApiRequestTest {

    @Test
    public void testGetMemberByIdNotNull() throws BlackoutException {
        Member member
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.1.id")), Member.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(member);
    }

    @Test
    public void testGetMultipleMembersByIdNotNull() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("member.1.id")));
        memberIds.add(new Long(System.getProperty("member.2.id")));
        List<Member> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Member.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetMemberByIdCorrectMember() throws BlackoutException {
        Member member
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.1.id")), Member.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(member.getMemberId().equals(new Long(System.getProperty("member.1.id"))));
        assertNotNull(member.getUserName());
        assertNotNull(member.getRealName());
        assertNotNull(member.getEmail());
        assertNotNull(member.getMemberLevel());
        assertNotNull(member.getTeamId());
        assertNotNull(member.getCash());
        assertNotNull(member.getActive());
        assertNotNull(member.getRenames());
        assertNotNull(member.getMoves());
        assertNotNull(member.getSurveyed());
        assertNotNull(member.getRegMethod());
        assertNotNull(member.getTeamCountryIso());
        assertNotNull(member.getMainTeamId());
        assertNotNull(member.getLastClick());
        assertNotNull(member.getDateRegistered());
        assertNotNull(member.getTeams());
        assertFalse(member.getTeams().isEmpty());
    }

    @Test
    public void testGetCorrectMultipleMembersById() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("member.1.id")));
        memberIds.add(new Long(System.getProperty("member.2.id")));
        List<Member> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Member.class, Boolean.FALSE, Boolean.FALSE);

        Member member1 = entitiesFromApi.get(0);
        Member member2 = entitiesFromApi.get(1);

        assertTrue(member1.getMemberId().equals(new Long(System.getProperty("member.1.id"))));
        assertNotNull(member1.getUserName());
        assertNotNull(member1.getRealName());
        assertNotNull(member1.getEmail());
        assertNotNull(member1.getMemberLevel());
        assertNotNull(member1.getTeamId());
        assertNotNull(member1.getCash());
        assertNotNull(member1.getActive());
        assertNotNull(member1.getRenames());
        assertNotNull(member1.getMoves());
        assertNotNull(member1.getSurveyed());
        assertNotNull(member1.getRegMethod());
        assertNotNull(member1.getTeamCountryIso());
        assertNotNull(member1.getMainTeamId());
        assertNotNull(member1.getLastClick());
        assertNotNull(member1.getDateRegistered());
        assertNotNull(member1.getTeams());
        assertFalse(member1.getTeams().isEmpty());

        assertTrue(member2.getMemberId().equals(new Long(System.getProperty("member.2.id"))));
        assertNotNull(member2.getUserName());
        assertNotNull(member2.getTeamId());
        assertNotNull(member2.getActive());
        assertNotNull(member2.getRenames());
        assertNotNull(member2.getMoves());
        assertNotNull(member2.getSurveyed());
        assertNotNull(member2.getRegMethod());
        assertNotNull(member2.getTeamCountryIso());
        assertNotNull(member2.getMainTeamId());
        assertNotNull(member2.getLastClick());
        assertNotNull(member2.getDateRegistered());
        assertNotNull(member2.getTeams());
        assertFalse(member2.getTeams().isEmpty());
    }
}
