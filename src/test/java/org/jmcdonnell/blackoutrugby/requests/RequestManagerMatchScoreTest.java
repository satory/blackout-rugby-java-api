/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.beans.MatchScore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerMatchScoreTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalMatchScoreByIdNotNull() throws BlackoutException {
        MatchScore team
                = requestManager.getEntityFromApi(new Long(6537), MatchScore.class, Boolean.TRUE, Boolean.FALSE);
        assertNotNull(team);
    }

    @Test
    public void testGetUnderTwentyMatchScoreByIdNotNull() throws BlackoutException {
        MatchScore team
                = requestManager.getEntityFromApi(new Long(3716), MatchScore.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(team);
    }

    @Test
    public void testGetClubMatchScoreByIdNotNull() throws BlackoutException {
        MatchScore team
                = requestManager.getEntityFromApi(new Long(System.getProperty("match.saved.id")), MatchScore.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(team);
    }

    @Test
    public void testGetYouthMatchScoreByIdNotNull() throws BlackoutException {
        MatchScore team
                = requestManager.getEntityFromApi(new Long(10023659), MatchScore.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(team);
    }

    @Test
    public void testGetMatchScoreById() throws BlackoutException {
        MatchScore matchScore
                = requestManager.getEntityFromApi(new Long(System.getProperty("match.saved.id")), MatchScore.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(matchScore.isMatchStarted());
        assertTrue(matchScore.isMatchFinished());
        assertTrue(matchScore.getHomeScore().equals(new Integer(System.getProperty("match.saved.homescore"))));
        assertTrue(matchScore.getGuestScore().equals(new Integer(System.getProperty("match.saved.guestscore"))));
        assertNotNull(matchScore.getGameTimeInSecs());
    }
}
