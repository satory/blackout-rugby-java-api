/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.beans.Lineup;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerLineupsTest extends AbstractApiRequestTest {

    @Test
    public void testGetClubLineupNotNull() throws BlackoutException {
        Lineup team
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), Lineup.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(team);
    }

    @Test
    public void testGetYouthLineupNotNull() throws BlackoutException {
        Lineup team
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), Lineup.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(team);
    }

    @Test
    public void testGetDefaultLineup() throws BlackoutException {
        Lineup lineup
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), Lineup.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(lineup.getB1());
        assertNotNull(lineup.getB2());
        assertNotNull(lineup.getB3());
        assertNotNull(lineup.getB4());
        assertNotNull(lineup.getB5());
        assertNotNull(lineup.getB6());
        assertNotNull(lineup.getB7());

        assertNotNull(lineup.getP1());
        assertNotNull(lineup.getP2());
        assertNotNull(lineup.getP3());
        assertNotNull(lineup.getP4());
        assertNotNull(lineup.getP5());
        assertNotNull(lineup.getP6());
        assertNotNull(lineup.getP7());
        assertNotNull(lineup.getP8());
        assertNotNull(lineup.getP9());
        assertNotNull(lineup.getP10());
        assertNotNull(lineup.getP11());
        assertNotNull(lineup.getP12());
        assertNotNull(lineup.getP13());
        assertNotNull(lineup.getP14());
        assertNotNull(lineup.getP15());

        assertNotNull(lineup.getCaptain());
        //assertNotNull(lineup.getDeadline());  //no deadline as this test uses the default lineup
        assertNotNull(lineup.getFixtureId());
        assertNotNull(lineup.getId());
        assertNotNull(lineup.getKicker());
        assertNotNull(lineup.getTeamId());

        assertNotNull(lineup.getR1p1());
        assertNotNull(lineup.getR1p2());
        assertNotNull(lineup.getR1p3());
        assertNotNull(lineup.getR1p4());
        assertNotNull(lineup.getR1p5());
        assertNotNull(lineup.getR1p6());
        assertNotNull(lineup.getR1p7());
        assertNotNull(lineup.getR1p8());
        assertNotNull(lineup.getR1p9());
        assertNotNull(lineup.getR1p10());
        assertNotNull(lineup.getR1p11());
        assertNotNull(lineup.getR1p12());
        assertNotNull(lineup.getR1p13());
        assertNotNull(lineup.getR1p14());
        assertNotNull(lineup.getR1p15());

        assertNotNull(lineup.getR2p1());
        assertNotNull(lineup.getR2p2());
        assertNotNull(lineup.getR2p3());
        assertNotNull(lineup.getR2p4());
        assertNotNull(lineup.getR2p5());
        assertNotNull(lineup.getR2p6());
        assertNotNull(lineup.getR2p7());
        assertNotNull(lineup.getR2p8());
        assertNotNull(lineup.getR2p9());
        assertNotNull(lineup.getR2p10());
        assertNotNull(lineup.getR2p11());
        assertNotNull(lineup.getR2p12());
        assertNotNull(lineup.getR2p13());
        assertNotNull(lineup.getR2p14());
        assertNotNull(lineup.getR2p15());

        assertNotNull(lineup.getCaptain1());
        assertNotNull(lineup.getCaptain2());

        assertNotNull(lineup.getKicker1());
        assertNotNull(lineup.getKicker2());

        assertNotNull(lineup.getCreative());
        assertNotNull(lineup.getDriving());
        assertNotNull(lineup.getPickAndGo());
        assertNotNull(lineup.getExpansive());

        assertNotNull(lineup.getKicking());
        assertNotNull(lineup.getKickForTouch());
        assertNotNull(lineup.getDropGoals());
        assertNotNull(lineup.getUpAndUnder());

        assertNotNull(lineup.getDiscipline());
        assertNotNull(lineup.getIntensity());
        assertNotNull(lineup.getDefense());
    }

    @Test
    public void testGetLineupForTeamIdAndFixtureIdNotNull() throws BlackoutException {
        Long teamId = new Long(System.getProperty("member.team.id"));
        Long fixtureId = new Long(System.getProperty("match.saved.id"));
        Lineup entityFromApi
                = requestManager.getLineupForTeamAndFixture(teamId, fixtureId, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entityFromApi);
    }

    @Test
    public void testGetCorrectLineupForTeamIdAndFixtureId() throws BlackoutException {
        Long teamId = new Long(System.getProperty("member.team.id"));
        Long fixtureId = new Long(System.getProperty("match.saved.id"));
        Lineup lineup
                = requestManager.getLineupForTeamAndFixture(teamId, fixtureId, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(lineup.getB1());
        assertNotNull(lineup.getB2());
        assertNotNull(lineup.getB3());
        assertNotNull(lineup.getB4());
        assertNotNull(lineup.getB5());
        assertNotNull(lineup.getB6());
        assertNotNull(lineup.getB7());

        assertNotNull(lineup.getP1());
        assertNotNull(lineup.getP2());
        assertNotNull(lineup.getP3());
        assertNotNull(lineup.getP4());
        assertNotNull(lineup.getP5());
        assertNotNull(lineup.getP6());
        assertNotNull(lineup.getP7());
        assertNotNull(lineup.getP8());
        assertNotNull(lineup.getP9());
        assertNotNull(lineup.getP10());
        assertNotNull(lineup.getP11());
        assertNotNull(lineup.getP12());
        assertNotNull(lineup.getP13());
        assertNotNull(lineup.getP14());
        assertNotNull(lineup.getP15());

        assertNotNull(lineup.getCaptain());
        assertNotNull(lineup.getDeadline());  //no deadline as this test uses the default lineup
        assertNotNull(lineup.getFixtureId());
        assertNotNull(lineup.getId());
        assertNotNull(lineup.getKicker());
        assertNotNull(lineup.getTeamId());

        assertNotNull(lineup.getR1p1());
        assertNotNull(lineup.getR1p2());
        assertNotNull(lineup.getR1p3());
        assertNotNull(lineup.getR1p4());
        assertNotNull(lineup.getR1p5());
        assertNotNull(lineup.getR1p6());
        assertNotNull(lineup.getR1p7());
        assertNotNull(lineup.getR1p8());
        assertNotNull(lineup.getR1p9());
        assertNotNull(lineup.getR1p10());
        assertNotNull(lineup.getR1p11());
        assertNotNull(lineup.getR1p12());
        assertNotNull(lineup.getR1p13());
        assertNotNull(lineup.getR1p14());
        assertNotNull(lineup.getR1p15());

        assertNotNull(lineup.getR2p1());
        assertNotNull(lineup.getR2p2());
        assertNotNull(lineup.getR2p3());
        assertNotNull(lineup.getR2p4());
        assertNotNull(lineup.getR2p5());
        assertNotNull(lineup.getR2p6());
        assertNotNull(lineup.getR2p7());
        assertNotNull(lineup.getR2p8());
        assertNotNull(lineup.getR2p9());
        assertNotNull(lineup.getR2p10());
        assertNotNull(lineup.getR2p11());
        assertNotNull(lineup.getR2p12());
        assertNotNull(lineup.getR2p13());
        assertNotNull(lineup.getR2p14());
        assertNotNull(lineup.getR2p15());

        assertNotNull(lineup.getCaptain1());
        assertNotNull(lineup.getCaptain2());

        assertNotNull(lineup.getKicker1());
        assertNotNull(lineup.getKicker2());

        assertNotNull(lineup.getCreative());
        assertNotNull(lineup.getDriving());
        assertNotNull(lineup.getPickAndGo());
        assertNotNull(lineup.getExpansive());

        assertNotNull(lineup.getKicking());
        assertNotNull(lineup.getKickForTouch());
        assertNotNull(lineup.getDropGoals());
        assertNotNull(lineup.getUpAndUnder());

        assertNotNull(lineup.getDiscipline());
        assertNotNull(lineup.getIntensity());
        assertNotNull(lineup.getDefense());
    }
}
