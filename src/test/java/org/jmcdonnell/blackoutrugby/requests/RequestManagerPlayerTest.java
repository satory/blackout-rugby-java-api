/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.beans.Player;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerPlayerTest extends AbstractApiRequestTest {

    public static final String LEFT = "Left";

    @Test
    public void testGetNationalPlayerByIdNotNull() throws BlackoutException {
        Player player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.national.id")), Player.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(player);
    }

    @Test
    public void testGetUnderTwentyPlayerByIdNotNull() throws BlackoutException {
        Player player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.undertwenty.id")), Player.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(player);
    }

    @Test
    public void testGetClubPlayerByIdNotNull() throws BlackoutException {
        Player player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.1.id")), Player.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(player);
    }

    @Test
    public void testGetYouthPlayerByIdNotNull() throws BlackoutException {
        Player player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.youth.id")), Player.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(player);
    }

    @Test
    public void testGetMultiplePlayersByIdNotNull() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("player.1.id")));
        memberIds.add(new Long(System.getProperty("player.2.id")));
        List<Player> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Player.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetPlayerByIdCorrectPlayer() throws BlackoutException {
        Player player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.1.id")), Player.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(player.getPlayerId().equals(new Long(System.getProperty("player.1.id"))));
        assertNotNull(player.getTeamId());
        assertNotNull(player.getfName());
        assertNotNull(player.getlName());
        assertNotNull(player.getAge());
        assertNotNull(player.getBirthday().equals("01:7"));
        assertNotNull(player.getHand().equals("Right"));
        assertNotNull(player.getFoot().equals(LEFT));
        assertNotNull(player.getNationality().equals("IE"));
        assertNotNull(player.getCsr());
        assertNotNull(player.getSalary());
        assertNotNull(player.getStamina());
        assertNotNull(player.getHandling());
        assertNotNull(player.getAttack());
        assertNotNull(player.getDefense());
        assertNotNull(player.getTechnique());
        assertNotNull(player.getStrength());
        assertNotNull(player.getJumping());
        assertNotNull(player.getSpeed());
        assertNotNull(player.getAgility());
        assertNotNull(player.getKicking());
        assertNotNull(player.getForm());
        assertNotNull(player.getAggression());
        assertNotNull(player.getDiscipline());
        assertNotNull(player.getEnergy());
        assertNotNull(player.getLeadership());
        assertNotNull(player.getExperience());
        assertNotNull(player.getWeight());
        assertNotNull(player.getHeight());
        assertNotNull(player.getYouthId());
        assertNotNull(player.getJersey());
        assertNotNull(player.getDualNationality());
        assertNotNull(player.getName());
        assertNotNull(player.getInjured());
        assertNotNull(player.getContract());
        assertNotNull(player.getJoined());
        assertNotNull(player.getPops());
    }

    @Test
    public void testGetCorrectMultipleMembersById() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("player.1.id")));
        memberIds.add(new Long(System.getProperty("player.2.id")));
        List<Player> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Player.class, Boolean.FALSE, Boolean.FALSE);

        Player player1 = entitiesFromApi.get(0);
        Player player2 = entitiesFromApi.get(1);

        assertTrue(player1.getPlayerId().equals(new Long(10740278)));
        assertNotNull(player1.getTeamId());
        assertNotNull(player1.getfName());
        assertNotNull(player1.getlName());
        assertNotNull(player1.getAge());
        assertNotNull(player1.getBirthday());
        assertNotNull(player1.getHand());
        assertNotNull(player1.getFoot());
        assertNotNull(player1.getNationality());
        assertNotNull(player1.getCsr());
        assertNotNull(player1.getSalary());
        assertNotNull(player1.getStamina());
        assertNotNull(player1.getHandling());
        assertNotNull(player1.getAttack());
        assertNotNull(player1.getDefense());
        assertNotNull(player1.getTechnique());
        assertNotNull(player1.getStrength());
        assertNotNull(player1.getJumping());
        assertNotNull(player1.getSpeed());
        assertNotNull(player1.getAgility());
        assertNotNull(player1.getKicking());
        assertNotNull(player1.getForm());
        assertNotNull(player1.getAggression());
        assertNotNull(player1.getDiscipline());
        assertNotNull(player1.getEnergy());
        assertNotNull(player1.getLeadership());
        assertNotNull(player1.getExperience());
        assertNotNull(player1.getWeight());
        assertNotNull(player1.getHeight());
        assertNotNull(player1.getYouthId());
        assertNotNull(player1.getJersey());
        assertNotNull(player1.getDualNationality());
        assertNotNull(player1.getName());
        assertNotNull(player1.getInjured());
        assertNotNull(player1.getContract());
        assertNotNull(player1.getJoined());
        assertNotNull(player1.getPops());

        assertTrue(player2.getPlayerId().equals(new Long(12176168)));
        assertNotNull(player2.getTeamId());
        assertNotNull(player2.getfName());
        assertNotNull(player2.getlName());
        assertNotNull(player2.getAge());
        assertNotNull(player2.getBirthday());
        assertNotNull(player2.getHand());
        assertNotNull(player2.getFoot());
        assertNotNull(player2.getNationality());
        assertNotNull(player2.getCsr());
        assertNotNull(player2.getSalary());
        assertNotNull(player2.getStamina());
        assertNotNull(player2.getHandling());
        assertNotNull(player2.getAttack());
        assertNotNull(player2.getDefense());
        assertNotNull(player2.getTechnique());
        assertNotNull(player2.getStrength());
        assertNotNull(player2.getJumping());
        assertNotNull(player2.getSpeed());
        assertNotNull(player2.getAgility());
        assertNotNull(player2.getKicking());
        assertNotNull(player2.getForm());
        assertNotNull(player2.getAggression());
        assertNotNull(player2.getDiscipline());
        assertNotNull(player2.getEnergy());
        assertNotNull(player2.getLeadership());
        assertNotNull(player2.getExperience());
        assertNotNull(player2.getWeight());
        assertNotNull(player2.getHeight());
        assertNotNull(player2.getYouthId());
        assertNotNull(player2.getJersey());
        assertNotNull(player2.getDualNationality());
        assertNotNull(player2.getName());
        assertNotNull(player2.getInjured());
        assertNotNull(player2.getContract());
        assertNotNull(player2.getJoined());
        assertNotNull(player2.getPops());
    }

    @Test
    public void testGetPlayersByTeamIdNotNull() throws BlackoutException {
        List<Player> teamPlayers = requestManager.getPlayersByTeamId(new Long(System.getProperty("member.team.id")), Boolean.FALSE, Boolean.FALSE);

        assertNotNull(teamPlayers);
    }

    @Test
    public void testGetPlayersByTeamIdOverMinSize() throws BlackoutException {
        List<Player> teamPlayers = requestManager.getPlayersByTeamId(new Long(System.getProperty("member.team.id")), Boolean.FALSE, Boolean.FALSE);

        assertTrue(teamPlayers.size() >= 22);
    }
}
