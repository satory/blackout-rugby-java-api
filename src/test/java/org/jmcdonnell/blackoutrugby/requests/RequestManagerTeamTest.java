/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.beans.Team;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerTeamTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalTeamByIdNotNull() throws BlackoutException {
        Team team
                = requestManager.getEntityFromApi(new Long(System.getProperty("nationalTeamId")), Team.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(team);
    }

    @Test
    public void testGetUnderTwentyTeamByIdNotNull() throws BlackoutException {
        Team team
                = requestManager.getEntityFromApi(new Long(System.getProperty("nationalTeamId")), Team.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(team);
    }

    @Test
    public void testGetYouthTeamByIdNotNull() throws BlackoutException {
        Team team
                = requestManager.getEntityFromApi(api.getTeamId(), Team.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(team);
    }

    @Test
    public void testGetClubTeamByIdNotNull() throws BlackoutException {
        Team team
                = requestManager.getEntityFromApi(api.getTeamId(), Team.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(team);
    }

    @Test
    public void testGetMultipleTeamsByIdNotNull() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(api.getTeamId());
        memberIds.add(new Long(45341));
        List<Team> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Team.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetTeamById() throws BlackoutException {
        Team team
                = requestManager.getEntityFromApi(api.getTeamId(), Team.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(team.getTeamId().equals(api.getTeamId()));
        assertNotNull(team.getTeamName());
        assertNotNull(team.getOwnerId());
        assertNotNull(team.getCountryIso());
        assertNotNull(team.getRegion());
        assertNotNull(team.getStadium());
        assertNotNull(team.isBot());
        assertNotNull(team.getContentment());
        assertNotNull(team.getMembers());
        assertNotNull(team.getStadiumCapacity());
        assertNotNull(team.getStadiumStanding());
        assertNotNull(team.getStadiumUncovered());
        assertNotNull(team.getStadiumCovered());
        assertNotNull(team.getStadiumMembers());
        assertNotNull(team.getStadiumCorporate());
        assertNotNull(team.getBankBalance());
        assertNotNull(team.getNickName1());
        assertNotNull(team.getNickName2());
        assertNotNull(team.getNickName3());
        assertNotNull(team.getRankingPoints());
        assertNotNull(team.getRegionRank());
        assertNotNull(team.getNationalRank());
        assertNotNull(team.getWorldRank());
        assertNotNull(team.getMinorSponsor());
        assertNotNull(team.getPrevRankingPoints());
        assertNotNull(team.getPrevRegionRank());
        assertNotNull(team.getPrevNationalRank());
        assertNotNull(team.getPrevWorldRank());
        assertNotNull(team.getScoutingStars());
        assertNotNull(team.getPlural());
        assertNotNull(team.getPluralNickname1());
        assertNotNull(team.getPluralNickname2());
        assertNotNull(team.getPluralNickname3());
        assertNotNull(team.getMinSalary());
        assertNotNull(team.getPremium());
        assertNotNull(team.getDateTakenOver());
        assertNotNull(team.getLeagueId());
        assertNotNull(team.getManager());
    }

    @Test
    public void testGetCorrectMultipleMembersById() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(api.getTeamId());
        memberIds.add(new Long(45341));
        List<Team> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, Team.class, Boolean.FALSE, Boolean.FALSE);

        Team team1 = entitiesFromApi.get(0);
        Team team2 = entitiesFromApi.get(1);

        assertTrue(team1.getTeamId().equals(api.getTeamId()));
        assertNotNull(team1.getTeamName());
        assertNotNull(team1.getOwnerId());
        assertNotNull(team1.getCountryIso());
        assertNotNull(team1.getRegion());
        assertNotNull(team1.getStadium());
        assertNotNull(team1.isBot());
        assertNotNull(team1.getContentment());
        assertNotNull(team1.getMembers());
        assertNotNull(team1.getStadiumCapacity());
        assertNotNull(team1.getStadiumStanding());
        assertNotNull(team1.getStadiumUncovered());
        assertNotNull(team1.getStadiumCovered());
        assertNotNull(team1.getStadiumMembers());
        assertNotNull(team1.getStadiumCorporate());
        assertNotNull(team1.getBankBalance());
        assertNotNull(team1.getNickName1());
        assertNotNull(team1.getNickName2());
        assertNotNull(team1.getNickName3());
        assertNotNull(team1.getRankingPoints());
        assertNotNull(team1.getRegionRank());
        assertNotNull(team1.getNationalRank());
        assertNotNull(team1.getWorldRank());
        assertNotNull(team1.getMinorSponsor());
        assertNotNull(team1.getPrevRankingPoints());
        assertNotNull(team1.getPrevRegionRank());
        assertNotNull(team1.getPrevNationalRank());
        assertNotNull(team1.getPrevWorldRank());
        assertNotNull(team1.getScoutingStars());
        assertNotNull(team1.getPlural());
        assertNotNull(team1.getPluralNickname1());
        assertNotNull(team1.getPluralNickname2());
        assertNotNull(team1.getPluralNickname3());
        assertNotNull(team1.getMinSalary());
        assertNotNull(team1.getPremium());
        assertNotNull(team1.getDateTakenOver());
        assertNotNull(team1.getLeagueId());
        assertNotNull(team1.getManager());

        assertTrue(team2.getTeamId().equals(new Long(45341)));
        assertNotNull(team2.getTeamName());
        assertNotNull(team2.getOwnerId());
        assertNotNull(team2.getCountryIso());
        assertNotNull(team2.getRegion());
        assertNotNull(team2.getStadium());
        assertNotNull(team2.isBot());
        assertNotNull(team2.getNickName1());
        assertNotNull(team2.getNickName2());
        assertNotNull(team2.getNickName3());
        assertNotNull(team2.getRankingPoints());
        assertNotNull(team2.getRegionRank());
        assertNotNull(team2.getNationalRank());
        assertNotNull(team2.getWorldRank());
        assertNotNull(team2.getPrevRankingPoints());
        assertNotNull(team2.getPrevRegionRank());
        assertNotNull(team2.getPrevNationalRank());
        assertNotNull(team2.getPrevWorldRank());
        assertNotNull(team2.getPlural());
        assertNotNull(team2.getPluralNickname1());
        assertNotNull(team2.getPluralNickname2());
        assertNotNull(team2.getPluralNickname3());
        assertNotNull(team2.getMinSalary());
        assertNotNull(team2.getPremium());
        assertNotNull(team2.getDateTakenOver());
        assertNotNull(team2.getLeagueId());
        assertNotNull(team2.getManager());
    }
}
