/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import org.jmcdonnell.blackoutrugby.beans.TeamStatistics;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerTeamStatisiticsTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalTeamStatisicsByIdNotNull() throws BlackoutException {
        TeamStatistics teamStats
                = requestManager.getEntityFromApi(new Long(9), TeamStatistics.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(teamStats);
    }

    @Test
    public void testGetUnderTwentyTeamStatisicsByIdNotNull() throws BlackoutException {
        TeamStatistics teamStats
                = requestManager.getEntityFromApi(new Long(9), TeamStatistics.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(teamStats);
    }

    @Test
    public void testGetClubTeamStatisicsByIdNotNull() throws BlackoutException {
        TeamStatistics teamStats
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), TeamStatistics.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(teamStats);
    }

    @Test
    public void testGetYouthTeamStatisicsByIdNotNull() throws BlackoutException {
        TeamStatistics teamStats
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), TeamStatistics.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(teamStats);
    }

    @Test
    public void testGetTeamStatisticsById() throws BlackoutException {
        TeamStatistics playerStats
                = requestManager.getEntityFromApi(new Long(System.getProperty("member.team.id")), TeamStatistics.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(playerStats.getTeamId().equals(new Long(System.getProperty("member.team.id"))));
        assertNotNull(playerStats.getAverageKickingMetres());
        assertNotNull(playerStats.getAverageMinsInTwentyTwo());
        assertNotNull(playerStats.getBadKicks());
        assertNotNull(playerStats.getBadUpAndUnders());
        assertNotNull(playerStats.getBallTime());
        assertNotNull(playerStats.getConversions());
        assertNotNull(playerStats.getDropGoals());
        assertNotNull(playerStats.getFights());
        assertNotNull(playerStats.getForwardPasses());
        assertNotNull(playerStats.getGoodKicks());
        assertNotNull(playerStats.getGoodUpAndUnders());
        assertNotNull(playerStats.getHandlingErrors());
        assertNotNull(playerStats.getInjuries());
        assertNotNull(playerStats.getIntercepts());
        assertNotNull(playerStats.getKickingMetres());
        assertNotNull(playerStats.getKicks());
        assertNotNull(playerStats.getKicksOutOnTheFull());
        assertNotNull(playerStats.getKnockOns());
        assertNotNull(playerStats.getLineBreaks());
        assertNotNull(playerStats.getLineoutsAgainstThrow());
        assertNotNull(playerStats.getLineoutsLost());
        assertNotNull(playerStats.getLineoutsWon());
        assertNotNull(playerStats.getMatchesPlayed());
        assertNotNull(playerStats.getMaulsWon());
        assertNotNull(playerStats.getMetresGained());
        assertNotNull(playerStats.getMinsInTwentyTwo());
        assertNotNull(playerStats.getMissedConversions());
        assertNotNull(playerStats.getMissedDropGoals());
        assertNotNull(playerStats.getMissedPenalties());
        assertNotNull(playerStats.getMissedTackles());
        assertNotNull(playerStats.getPenalties());
        assertNotNull(playerStats.getPenaltiesConceded());
        assertNotNull(playerStats.getPenaltiesWon());
        assertNotNull(playerStats.getPenaltyTime());
        assertNotNull(playerStats.getPhases());
        assertNotNull(playerStats.getPossession());
        assertNotNull(playerStats.getRedCards());
        assertNotNull(playerStats.getRucksWon());
        assertNotNull(playerStats.getScrumsAgainstPutIn());
        assertNotNull(playerStats.getScrumsLost());
        assertNotNull(playerStats.getScrumsWon());
        assertNotNull(playerStats.getSevenPlusPhases());
        assertNotNull(playerStats.getTackles());
        assertNotNull(playerStats.getTerritory());
        assertNotNull(playerStats.getTotalPoints());
        assertNotNull(playerStats.getTries());
        assertNotNull(playerStats.getTurnovers());
        assertNotNull(playerStats.getTurnoversConceded());
        assertNotNull(playerStats.getUpAndUnders());
        assertNotNull(playerStats.getYellowCards());
    }
}
