/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.Standing;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class RequestManagerStandingTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalStandingByLeagueIdNotNull() throws BlackoutException {
        Long leagueId = new Long(System.getProperty("standing.id"));
        List<Standing> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(leagueId, Standing.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetUnderTwentyStandingByLeagueIdNotNull() throws BlackoutException {
        Long leagueId = new Long(System.getProperty("standing.id"));
        List<Standing> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(leagueId, Standing.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetClubStandingByLeagueIdNotNull() throws BlackoutException {
        Long leagueId = new Long(System.getProperty("standing.id"));
        List<Standing> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(leagueId, Standing.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetYouthStandingByLeagueIdNotNull() throws BlackoutException {
        Long leagueId = new Long(System.getProperty("standing.id"));
        List<Standing> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(leagueId, Standing.class, Boolean.FALSE, Boolean.TRUE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetStandingByLeagueId() throws BlackoutException {
        Long memberId = new Long(System.getProperty("standing.id"));
        List<Standing> entitiesFromApi
                = requestManager.getListOfEntityByIdFromApi(memberId, Standing.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(entitiesFromApi.size() == 8);
        Standing get = entitiesFromApi.get(0);

        assertNotNull(get.getB1());
        assertNotNull(get.getB2());
        assertNotNull(get.getCountryIso());
        assertNotNull(get.getDivision());
        assertNotNull(get.getDrawn());
        assertNotNull(get.getId());
        assertNotNull(get.getLeague());
        assertNotNull(get.getLost());
        assertNotNull(get.getLeagueId());
        assertNotNull(get.getPlayed());
        assertNotNull(get.getPoints());
        assertNotNull(get.getPointsAgainst());
        assertNotNull(get.getPointsFor());
        assertNotNull(get.getSeason());
        assertNotNull(get.getStanding());
        assertNotNull(get.getTeamId());
        assertNotNull(get.getWon());
    }
}
