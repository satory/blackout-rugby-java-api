/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.blackoutrugby.requests;

import java.util.ArrayList;
import java.util.List;
import org.jmcdonnell.blackoutrugby.beans.PlayerStatistics;
import org.jmcdonnell.blackoutrugby.exceptions.BlackoutException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author john
 */
public class RequestManagerPlayerStatisiticsTest extends AbstractApiRequestTest {

    @Test
    public void testGetNationalPlayerStatisticsByIdNotNull() throws BlackoutException {
        PlayerStatistics player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.national.id")), PlayerStatistics.class, Boolean.TRUE, Boolean.FALSE);

        assertNotNull(player);
    }

    @Test
    public void testGetUnderTwentyPlayerStatisticsByIdNotNull() throws BlackoutException {
        PlayerStatistics player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.undertwenty.id")), PlayerStatistics.class, Boolean.TRUE, Boolean.TRUE);

        assertNotNull(player);
    }

    @Test
    public void testGetClubPlayerStatisticsByIdNotNull() throws BlackoutException {
        PlayerStatistics player
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.1.id")), PlayerStatistics.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(player);
    }

    @Test
    public void testGetMultiplePlayerStatisticsByIdNotNull() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("player.1.id")));
        memberIds.add(new Long(System.getProperty("player.2.id")));
        List<PlayerStatistics> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, PlayerStatistics.class, Boolean.FALSE, Boolean.FALSE);

        assertNotNull(entitiesFromApi);
    }

    @Test
    public void testGetCorrectPlayerStatisticsById() throws BlackoutException {
        PlayerStatistics playerStats
                = requestManager.getEntityFromApi(new Long(System.getProperty("player.1.id")), PlayerStatistics.class, Boolean.FALSE, Boolean.FALSE);

        assertTrue(playerStats.getPlayerId().equals(new Long(System.getProperty("player.1.id"))));
        assertNotNull(playerStats.getAverageKickingMetres());
        assertNotNull(playerStats.getBadKicks());
        assertNotNull(playerStats.getBadUpAndUnders());
        assertNotNull(playerStats.getBallTime());
        assertNotNull(playerStats.getBeatenDefenders());
        assertNotNull(playerStats.getConversions());
        assertNotNull(playerStats.getCupCaps());
        assertNotNull(playerStats.getDropGoals());
        assertNotNull(playerStats.getFights());
        assertNotNull(playerStats.getForwardPasses());
        assertNotNull(playerStats.getFriendlyCaps());
        assertNotNull(playerStats.getGoodKicks());
        assertNotNull(playerStats.getGoodUpAndUnders());
        assertNotNull(playerStats.getHandlingErrors());
        assertNotNull(playerStats.getInjuries());
        assertNotNull(playerStats.getIntercepts());
        assertNotNull(playerStats.getKickingMetres());
        assertNotNull(playerStats.getKicks());
        assertNotNull(playerStats.getKicksOutOnTheFull());
        assertNotNull(playerStats.getKnockOns());
        assertNotNull(playerStats.getLeagueCaps());
        assertNotNull(playerStats.getLineBreaks());
        assertNotNull(playerStats.getLineoutsConceeded());
        assertNotNull(playerStats.getLineoutsSecured());
        assertNotNull(playerStats.getLineoutsStolen());
        assertNotNull(playerStats.getMetresGained());
        assertNotNull(playerStats.getMissedConversions());
        assertNotNull(playerStats.getMissedDropGoals());
        assertNotNull(playerStats.getMissedPenalties());
        assertNotNull(playerStats.getMissedTackles());
        assertNotNull(playerStats.getNationalCaps());
        assertNotNull(playerStats.getOtherCaps());
        assertNotNull(playerStats.getPenalties());
        assertNotNull(playerStats.getPenaltiesConceded());
        assertNotNull(playerStats.getPenaltyTime());
        assertNotNull(playerStats.getRedCards());
        assertNotNull(playerStats.getSuccessfulLineoutThrows());
        assertNotNull(playerStats.getTackles());
        assertNotNull(playerStats.getTotalPoints());
        assertNotNull(playerStats.getTries());
        assertNotNull(playerStats.getTryAssists());
        assertNotNull(playerStats.getTurnOversWon());
        assertNotNull(playerStats.getUnder20Caps());
        assertNotNull(playerStats.getUnder20WorldCupCaps());
        assertNotNull(playerStats.getUnsuccessfulLineoutThrows());
        assertNotNull(playerStats.getUpAndUnders());
        assertNotNull(playerStats.getWorldCupCaps());
        assertNotNull(playerStats.getYellowCards());
    }

    @Test
    public void testGetCorrectMultiplePlayerStatisiticsById() throws BlackoutException {
        List<Long> memberIds = new ArrayList<>();
        memberIds.add(new Long(System.getProperty("player.1.id")));
        memberIds.add(new Long(System.getProperty("player.2.id")));
        List<PlayerStatistics> entitiesFromApi
                = requestManager.getEntitiesFromApi(memberIds, PlayerStatistics.class, Boolean.FALSE, Boolean.FALSE);

        PlayerStatistics playerStats1 = entitiesFromApi.get(0);
        PlayerStatistics playerStats2 = entitiesFromApi.get(1);

        assertTrue(playerStats1.getPlayerId().equals(new Long(System.getProperty("player.1.id"))));
        assertNotNull(playerStats1.getAverageKickingMetres());
        assertNotNull(playerStats1.getBadKicks());
        assertNotNull(playerStats1.getBadUpAndUnders());
        assertNotNull(playerStats1.getBallTime());
        assertNotNull(playerStats1.getBeatenDefenders());
        assertNotNull(playerStats1.getConversions());
        assertNotNull(playerStats1.getCupCaps());
        assertNotNull(playerStats1.getDropGoals());
        assertNotNull(playerStats1.getFights());
        assertNotNull(playerStats1.getForwardPasses());
        assertNotNull(playerStats1.getFriendlyCaps());
        assertNotNull(playerStats1.getGoodKicks());
        assertNotNull(playerStats1.getGoodUpAndUnders());
        assertNotNull(playerStats1.getHandlingErrors());
        assertNotNull(playerStats1.getInjuries());
        assertNotNull(playerStats1.getIntercepts());
        assertNotNull(playerStats1.getKickingMetres());
        assertNotNull(playerStats1.getKicks());
        assertNotNull(playerStats1.getKicksOutOnTheFull());
        assertNotNull(playerStats1.getKnockOns());
        assertNotNull(playerStats1.getLeagueCaps());
        assertNotNull(playerStats1.getLineBreaks());
        assertNotNull(playerStats1.getLineoutsConceeded());
        assertNotNull(playerStats1.getLineoutsSecured());
        assertNotNull(playerStats1.getLineoutsStolen());
        assertNotNull(playerStats1.getMetresGained());
        assertNotNull(playerStats1.getMissedConversions());
        assertNotNull(playerStats1.getMissedDropGoals());
        assertNotNull(playerStats1.getMissedPenalties());
        assertNotNull(playerStats1.getMissedTackles());
        assertNotNull(playerStats1.getNationalCaps());
        assertNotNull(playerStats1.getOtherCaps());
        assertNotNull(playerStats1.getPenalties());
        assertNotNull(playerStats1.getPenaltiesConceded());
        assertNotNull(playerStats1.getPenaltyTime());
        assertNotNull(playerStats1.getRedCards());
        assertNotNull(playerStats1.getSuccessfulLineoutThrows());
        assertNotNull(playerStats1.getTackles());
        assertNotNull(playerStats1.getTotalPoints());
        assertNotNull(playerStats1.getTries());
        assertNotNull(playerStats1.getTryAssists());
        assertNotNull(playerStats1.getTurnOversWon());
        assertNotNull(playerStats1.getUnder20Caps());
        assertNotNull(playerStats1.getUnder20WorldCupCaps());
        assertNotNull(playerStats1.getUnsuccessfulLineoutThrows());
        assertNotNull(playerStats1.getUpAndUnders());
        assertNotNull(playerStats1.getWorldCupCaps());
        assertNotNull(playerStats1.getYellowCards());

        assertTrue(playerStats2.getPlayerId().equals(new Long(System.getProperty("player.2.id"))));
        assertNotNull(playerStats2.getAverageKickingMetres());
        assertNotNull(playerStats2.getBadKicks());
        assertNotNull(playerStats2.getBadUpAndUnders());
        assertNotNull(playerStats2.getBallTime());
        assertNotNull(playerStats2.getBeatenDefenders());
        assertNotNull(playerStats2.getConversions());
        assertNotNull(playerStats2.getCupCaps());
        assertNotNull(playerStats2.getDropGoals());
        assertNotNull(playerStats2.getFights());
        assertNotNull(playerStats2.getForwardPasses());
        assertNotNull(playerStats2.getFriendlyCaps());
        assertNotNull(playerStats2.getGoodKicks());
        assertNotNull(playerStats2.getGoodUpAndUnders());
        assertNotNull(playerStats2.getHandlingErrors());
        assertNotNull(playerStats2.getInjuries());
        assertNotNull(playerStats2.getIntercepts());
        assertNotNull(playerStats2.getKickingMetres());
        assertNotNull(playerStats2.getKicks());
        assertNotNull(playerStats2.getKicksOutOnTheFull());
        assertNotNull(playerStats2.getKnockOns());
        assertNotNull(playerStats2.getLeagueCaps());
        assertNotNull(playerStats2.getLineBreaks());
        assertNotNull(playerStats2.getLineoutsConceeded());
        assertNotNull(playerStats2.getLineoutsSecured());
        assertNotNull(playerStats2.getLineoutsStolen());
        assertNotNull(playerStats2.getMetresGained());
        assertNotNull(playerStats2.getMissedConversions());
        assertNotNull(playerStats2.getMissedDropGoals());
        assertNotNull(playerStats2.getMissedPenalties());
        assertNotNull(playerStats2.getMissedTackles());
        assertNotNull(playerStats2.getNationalCaps());
        assertNotNull(playerStats2.getOtherCaps());
        assertNotNull(playerStats2.getPenalties());
        assertNotNull(playerStats2.getPenaltiesConceded());
        assertNotNull(playerStats2.getPenaltyTime());
        assertNotNull(playerStats2.getRedCards());
        assertNotNull(playerStats2.getSuccessfulLineoutThrows());
        assertNotNull(playerStats2.getTackles());
        assertNotNull(playerStats2.getTotalPoints());
        assertNotNull(playerStats2.getTries());
        assertNotNull(playerStats2.getTryAssists());
        assertNotNull(playerStats2.getTurnOversWon());
        assertNotNull(playerStats2.getUnder20Caps());
        assertNotNull(playerStats2.getUnder20WorldCupCaps());
        assertNotNull(playerStats2.getUnsuccessfulLineoutThrows());
        assertNotNull(playerStats2.getUpAndUnders());
        assertNotNull(playerStats2.getWorldCupCaps());
        assertNotNull(playerStats2.getYellowCards());
    }
}
